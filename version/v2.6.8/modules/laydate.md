# 日期和时间组件文档 - layui.laydate

> 如你所见，layDate 在 layui 2.0 的版本中迎来一次重生。无论曾经它给你带来过多么糟糕的体验，从今往后，所有的旧坑都将弥合。全面重写的 layDate 包含了大量的更新，其中主要以：年选择器、年月选择器、日期选择器、时间选择器、日期时间选择器 五种类型的选择方式为基本核心，并且均支持范围选择（即双控件）。内置强劲的自定义日期格式解析和合法校正机制，含中文版和国际版，主题简约却又不失灵活多样。由于内部采用的是零依赖的原生 JavaScript 编写，因此又可作为独立组件使用。毫无疑问，这是 layui 的虔心之作。

> 模块加载名称：laydate，独立版本：http://www.layui.com/laydate/

## 快速使用

和 layer 一样，你可以在 layui 中使用 layDate，也可直接使用 layDate 独立版，请按照你的实际需求来选择。

| 场景                   | 用前准备                                                                                 | 调用方式                                                   |
| :--------------------- | :--------------------------------------------------------------------------------------- | :--------------------------------------------------------- |
| 1. 在 layui 模块中使用 | 下载 layui 后，引入*layui.css*和*layui.js*即可                                           | 通过*layui.use('laydate', callback)*加载模块后，再调用方法 |
| 2. 作为独立组件使用    | 去 [layDate](http://www.layui.com/laydate/) 独立版本官网下载组件包，引入 laydate.js 即可 | 直接调用方法使用                                           |

这是一个最简单的示例：

[](./example/laydate.html ':include :type=iframe height=320px')

在 layui 模块中使用

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>layDate快速使用</title>
        <link rel="stylesheet" href="/static/build/layui.css" media="all" />
    </head>
    <body>
        <div class="layui-inline">
            <!-- 注意：这一层元素并不是必须的 -->
            <input type="text" class="layui-input" id="test1" />
        </div>

        <script src="/static/build/layui.js"></script>
        <script>
            layui.use('laydate', function () {
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '#test1', //指定元素
                });
            });
        </script>
    </body>
</html>
```

作为独立组件使用

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>使用 layDate 独立版</title>
    </head>
    <body>
        <input type="text" id="test1" />

        <script src="laydate.js"></script>
        <script>
            //执行一个laydate实例
            laydate.render({
                elem: '#test1', //指定元素
            });
        </script>
    </body>
</html>
```

除了在组件加载方式有一些小小的不同，其它都完全类似

## 基础参数选项

通过核心方法：`laydate.render(options)` 来设置基础参数，也可以通过方法：`laydate.set(options)` 来设定全局基础参数.

## elem - 绑定元素

类型：String/DOM，默认值：无

必填项，用于绑定执行日期渲染的元素，值一般为选择器，或 DOM 对象

```javascript
laydate.render({ elem: '#test' //或 elem: document.getElementById('test')、elem: lay('#test') 等 });
```

## type - 控件选择类型

类型：String，默认值：date

用于单独提供不同的选择器类型，可选值如下表：

| type 可选值 | 名称           | 用途                                        |
| :---------- | :------------- | :------------------------------------------ |
| year        | 年选择器       | 只提供年列表选择                            |
| month       | 年月选择器     | 只提供年、月选择                            |
| date        | 日期选择器     | 可选择：年、月、日。type 默认值，一般可不填 |
| time        | 时间选择器     | 只提供时、分、秒选择                        |
| datetime    | 日期时间选择器 | 可选择：年、月、日、时、分、秒              |

```javascript
//年选择器
laydate.render({
    elem: '#test',
    type: 'year',
});

//年月选择器
laydate.render({
    elem: '#test',
    type: 'month',
});

//日期选择器
laydate.render({
    elem: '#test',
    //,type: 'date' //默认，可不填
});

//时间选择器
laydate.render({
    elem: '#test',
    type: 'time',
});

//日期时间选择器
laydate.render({
    elem: '#test',
    type: 'datetime',
});
```

## range - 开启左右面板范围选择

类型：Boolean/String/Array，默认值：false

如果设置 true，将默认采用 “ - ” 分割。 你也可以直接设置 分割字符。五种选择器类型均支持左右面板的范围选择。

```javascript
//日期范围选择
laydate.render({
    elem: '#test',
    range: true, //或 range: '~' 来自定义分割字符
});

//日期时间范围选择
laydate.render({
    elem: '#test',
    type: 'datetime',
    range: true,
});

//时间范围选择
laydate.render({
    elem: '#test',
    type: 'time',
    range: true,
});

//年范围选择
laydate.render({
    elem: '#test',
    type: 'year',
    range: true,
});

//年月范围选择
laydate.render({
    elem: '#test',
    type: 'month',
    range: true,
});
```

如果您要将开始时间和结束时间分开，那么还可以将 range 参数设置为数组，如：

```html
<div class="layui-form-item">
    <div class="layui-inline">
        <label class="layui-form-label">日期范围</label>
        <div class="layui-inline" id="test-range">
            <div class="layui-input-inline">
                <input type="text" id="startDate" class="layui-input" placeholder="开始日期" />
            </div>
            <div class="layui-form-mid">-</div>
            <div class="layui-input-inline">
                <input type="text" id="endDate" class="layui-input" placeholder="结束日期" />
            </div>
        </div>
    </div>
</div>
<script>
    laydate.render({
        elem: '#test-range', //开始时间和结束时间所在 input 框的父选择器
        //设置开始日期、日期日期的 input 选择器
        range: ['#startDate', '#endDate'], //数组格式为 layui 2.6.6 开始新增
    });
</script>
```

## format - 自定义格式

类型：String，默认值：yyyy-MM-dd

通过日期时间各自的格式符和长度，来设定一个你所需要的日期格式。layDate 支持的格式如下：

| 格式符 | 说明                                             |
| :----- | :----------------------------------------------- |
| yyyy   | 年份，至少四位数。如果不足四位，则前面补零       |
| y      | 年份，不限制位数，即不管年份多少位，前面均不补零 |
| MM     | 月份，至少两位数。如果不足两位，则前面补零。     |
| M      | 月份，允许一位数。                               |
| dd     | 日期，至少两位数。如果不足两位，则前面补零。     |
| d      | 日期，允许一位数。                               |
| HH     | 小时，至少两位数。如果不足两位，则前面补零。     |
| H      | 小时，允许一位数。                               |
| mm     | 分钟，至少两位数。如果不足两位，则前面补零。     |
| m      | 分钟，允许一位数。                               |
| ss     | 秒数，至少两位数。如果不足两位，则前面补零。     |
| s      | 秒数，允许一位数。                               |

通过上述不同的格式符组合成一段日期时间字符串，可任意排版，如下所示：

| 格式                                  | 示例值                                |
| :------------------------------------ | :------------------------------------ |
| yyyy-MM-dd HH:mm:ss                   | 2017-08-18 20:08:08                   |
| yyyy 年 MM 月 dd 日 HH 时 mm 分 ss 秒 | 2017 年 08 月 18 日 20 时 08 分 08 秒 |
| yyyyMMdd                              | 20170818                              |
| dd/MM/yyyy                            | 18/08/2017                            |
| yyyy 年 M 月                          | 2017 年 8 月                          |
| M 月 d 日                             | 8 月 18 日                            |
| 北京时间：HH 点 mm 分                 | 北京时间：20 点 08 分                 |
| yyyy 年的 M 月某天晚上，大概 H 点     | 2017 年的 8 月某天晚上，大概 20 点    |

```javascript
//自定义日期格式
laydate.render({
    elem: '#test',
    format: 'yyyy年MM月dd日', //可任意组合
});
```

## value - 初始值

类型：String，默认值：new Date()

支持传入符合 format 参数设定的日期格式字符，或者 new Date()

```javascript
//传入符合format格式的字符给初始值
laydate.render({
    elem: '#test',
    value: '2018-08-18', //必须遵循format参数设定的格式
});

//传入Date对象给初始值
laydate.render({
    elem: '#test',
    value: new Date(1534766888000), //参数即为：2018-08-20 20:08:08 的时间戳
});
```

## isInitValue - 初始值填充

类型：Boolean，默认值：true

用于控制是否自动向元素填充初始值（需配合 value 参数使用）

```javascript
laydate.render({
    elem: '#test',
    value: '2017-09-10',
    isInitValue: false, //是否允许填充初始值，默认为 true
});
```

> 注意：该参数为 layui 2.3.0 新增。

## isPreview - 是否开启选择值预览

类型：Boolean，默认值：true

用于控制是否显示当前结果的预览（type 为 datetime 时不开启）

```javascript
laydate.render({
    elem: '#test',
    isPreview: false, //禁用面板左下角选择值的预览，默认 true
});
```

> 注意：该参数为 layui 2.6.6 新增。

## min/max - 最小/大范围内的日期时间值

类型：string，默认值：min: '1900-1-1'、max: '2099-12-31'

设定有限范围内的日期或时间值，不在范围内的将不可选中。这两个参数的赋值非常灵活，主要有以下几种情况：

-   如果值为字符类型，则：年月日必须用 -（中划线）分割、时分秒必须用 :（半角冒号）号分割。这里并非遵循 format 设定的格式
-   如果值为整数类型，且数字＜ 86400000，则数字代表天数，如：min: -7，即代表最小日期在 7 天前，正数代表若干天后
-   如果值为整数类型，且数字 ≥ 86400000，则数字代表时间戳，如：max: 4073558400000，即代表最大日期在：公元 3000 年 1 月 1 日

```javascript
//日期有效范围只限定在：2017年
laydate.render({
    elem: '#test',
    min: '2017-1-1',
    max: '2017-12-31',
});

//日期有效范围限定在：过去一周到未来一周
laydate.render({
    elem: '#test',
    min: -7, //7天前
    max: 7, //7天后
});

//日期时间有效范围的设定:
laydate.render({
    elem: '#test',
    type: 'datetime',
    min: '2017-8-11 12:30:00',
    max: '2017-8-18 12:30:00',
});

//时间有效范围设定在: 上午九点半到下午五点半
laydate.render({
    elem: '#test',
    type: 'time',
    min: '09:30:00',
    max: '17:30:00',
});
```

毫不保留地说，min 和 max 参数是两个非常强大的存在，合理运用，可帮助用户在日期与时间的选择上带来更为友好的约束与体验。

## trigger - 自定义弹出控件的事件

类型：String，默认值：focus，如果绑定的元素非输入框，则默认事件为：click

```javascript
//自定义事件
laydate.render({
    elem: '#test',
    trigger: 'click', //采用click弹出
});
```

## show - 默认显示

类型：Boolean，默认值：false

如果设置: true，则控件默认显示在绑定元素的区域。通常用于外部事件调用控件，如：

```javascript
//默认显示
laydate.render({
    elem: '#test',
    show: true, //直接显示
});

//外部事件调用
lay('#test1').on('click', function (e) {
    //假设 test1 是一个按钮
    laydate.render({
        elem: '#test',
        show: true, //直接显示
        closeStop: '#test1', //这里代表的意思是：点击 test1 所在元素阻止关闭事件冒泡。如果不设定，则无法弹出控件
    });
});
```

## position - 定位方式

类型：String，默认值：absolute

用于设定控件的定位方式，有以下三种可选值：

| position 可选值 | 说明                                                                                                                                             |
| :-------------- | :----------------------------------------------------------------------------------------------------------------------------------------------- |
| abolute         | 绝对定位，始终吸附在绑定元素周围。默认值                                                                                                         |
| fixed           | 固定定位，初始吸附在绑定元素周围，不随浏览器滚动条所左右。_一般用于在固定定位的弹层中使用。_                                                     |
| static          | 静态定位，控件将直接嵌套在指定容器中。 注意：_请勿与 show 参数的概念搞混淆。show 为 true 时，控件仍然是采用绝对或固定定位。而这里是直接嵌套显示_ |

下面是一个直接嵌套显示的例子：

[](./example/laydate-static.html ':include :type=iframe height=360px')

[](./example/laydate-static.html ':include :type=code')

## zIndex - 层叠顺序

类型：Number，默认值：66666666

一般用于解决与其它元素的互相被遮掩的问题。如果 position 参数设为 static 时，该参数无效。

```javascript
//设定控件的层叠顺序
laydate.render({
    elem: '#test',
    zIndex: 99999999,
});
```

## showBottom - 是否显示底部栏

类型：Boolean，默认值：true

如果设置 false，将不会显示控件的底部栏区域

```javascript
//不显示底部栏
laydate.render({
    elem: '#test',
    showBottom: false,
});
```

## btns - 工具按钮

类型：Array，默认值：['clear', 'now', 'confirm']

右下角显示的按钮，会按照数组顺序排列，内置可识别的值有：clear、now、confirm

```javascript
//只显示清空和确认
laydate.render({
    elem: '#test',
    btns: ['clear', 'confirm'],
});
```

## lang - 语言

类型：String，默认值：cn

我们内置了两种语言版本：cn（中文版）、en（国际版，即英文版）。这里并没有开放自定义文字，是为了避免繁琐的配置。

```javascript
//国际版
laydate.render({
    elem: '#test',
    lang: 'en',
});
```

## theme - 主题

类型：String，默认值：default

我们内置了多种主题，theme 的可选值有：default（默认简约）、molv（墨绿背景）、#颜色值（自定义颜色背景）、grid（格子主题）

```javascript
//墨绿背景主题
laydate.render({
    elem: '#test',
    theme: 'molv',
});

//自定义背景色主题 - 非常实用
laydate.render({
    elem: '#test',
    theme: '#393D49',
});

//格子主题
laydate.render({
    elem: '#test',
    theme: 'grid',
});
```

另外，你还可以传入其它字符，如：theme: 'xxx'，那么控件将会多出一个 class="laydate-theme-xxx" 的 CSS 类，以便于你单独定制主题。

## calendar - 是否显示公历节日

类型：Boolean，默认值：false

我们内置了一些我国通用的公历重要节日，通过设置 true 来开启。国际版不会显示。

```javascript
//允许显示公历节日
laydate.render({
    elem: '#test',
    calendar: true,
});
```

## mark - 标注重要日子

类型：Object，默认值：无

calendar 参数所代表的公历节日更多情况下是一个摆设。因此，我们还需要自定义标注重要日子，比如结婚纪念日？日程等？它分为以下两种：

| 标注       | 格式                  | 说明                                                 |
| :--------- | :-------------------- | :--------------------------------------------------- |
| 每年的日期 | {'0-9-18': '国耻'}    | 0 即代表每一年                                       |
| 每月的日期 | {'0-0-15': '中旬'}    | 0-0 即代表每年每月（layui 2.1.1/layDate 5.0.4 新增） |
| 特定的日期 | {'2017-8-21': '发布') | -                                                    |

可同时设定多个，如：

```javascript
//标注重要日子
var ins1 = laydate.render({
    elem: '#test',
    mark: {
        '0-10-14': '生日',
        '0-12-31': '跨年', //每年12月31日
        '0-0-10': '工资', //每个月10号
        '2017-8-15': '', //具体日期
        '2017-8-20': '预发', //如果为空字符，则默认显示数字+徽章
        '2017-8-21': '发布',
    },
    done: function (value, date) {
        if (date.year === 2017 && date.month === 8 && date.date === 15) {
            //点击2017年8月15日，弹出提示语
            ins1.hint('中国人民抗日战争胜利72周年');
        }
    },
});
```

非常实用的存在，是时候利用它制作你的日程表了。

## 控件初始打开的回调

控件在打开时触发，回调返回一个参数：初始的日期时间对象

```javascript
laydate.render({
    elem: '#test',
    ready: function (date) {
        console.log(date); //得到初始的日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
    },
});
```

## 日期时间被切换后的回调

年月日时间被切换时都会触发。回调返回三个参数，分别代表：生成的值、日期时间对象、结束的日期时间对象

```javascript
laydate.render({
    elem: '#test',
    change: function (value, date, endDate) {
        console.log(value); //得到日期生成的值，如：2017-08-18
        console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
        console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
    },
});
```

## 控件选择完毕后的回调

点击日期、清空、现在、确定均会触发。回调返回三个参数，分别代表：生成的值、日期时间对象、结束的日期时间对象

```javascript
laydate.render({
    elem: '#test',
    done: function (value, date, endDate) {
        console.log(value); //得到日期生成的值，如：2017-08-18
        console.log(date); //得到日期时间对象：{year: 2017, month: 8, date: 18, hours: 0, minutes: 0, seconds: 0}
        console.log(endDate); //得结束的日期时间对象，开启范围选择（range: true）才会返回。对象成员同上。
    },
});
```

## 弹出控件提示

事实上，执行核心方法 laydate.render(options) 会返回一个当前实例对象。其中包含一些成员属性和方法，比如：hint 方法

```javascript
var ins1 = laydate.render({
    elem: '#test',
    change: function (value, date, endDate) {
        ins1.hint(value); //在控件上弹出value值
    },
});
```

## 配置基础路径

如果你不是采用 layui 或者普通 script 标签方式加载的 laydate.js，而是采用 requirejs 等其它方式引用 laydate，那么你需要设置基础路径，以便 laydate.css 完成加载。

```javascript
laydate.path = '/static/xxx/'; //laydate.js 所在目录

//配置好路径后，再调用
laydate.render(options);
```

除上述之外，如果您采用的是独立的 laydate，那么你还可以在动态加载 laydate 之前预先定义一个我们约定好的全局对象：

```html
<script>
    var LAYUI_GLOBAL = {
        laydate_dir: '/res/laydate/', //laydate 所在目录（laydate 5.3.0 开始新增）
    };
</script>
```

> 提示 1：上述只针对独立版 laydate，其 LAYUI_GLOBAL 设定的 laydate_dir 优先级高于 laydate.path；

> 提示 2：如果是 layui 加载的 laydate，可以无视上述所有的目录设定。前置工作都会在 layui 内部完成。

## 其它方法

| 方法名                          | 备注                                                                                                                                                                       |
| :------------------------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| laydate.getEndDate(month, year) | 获取指定年月的最后一天，month 默认为当前月，year 默认为当前年。如： var endDate1 = laydate.getEndDate(10); //得到 31 var endDate2 = laydate.getEndDate(2, 2080); //得到 29 |

## 结语

layDate 最早发布于 2014 年 6 月，但当时只迭代了一个版本，就再也没有更新。而时至今日，作为 layui 2.0 的核心组成之一，layDate 再度强势复活，不禁让人感慨万千！layDate 曾被我定义为：“最失败的一个组件”，被我无情搁置了整整三年。现在，是时候卸下这个标签了。

> layui - 在每一个细节中，用心与你沟通
