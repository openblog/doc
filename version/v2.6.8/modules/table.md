# table 数据表格文档 - layui.table

> table 模块是我们的又一走心之作，在 layui 2.0 的版本中全新推出，是 layui 最核心的组成之一。它用于对表格进行一些列功能和动态化数据操作，涵盖了日常业务所涉及的几乎全部需求。支持固定表头、固定行、固定列左/列右，支持拖拽改变列宽度，支持排序，支持多级表头，支持单元格的自定义模板，支持对表格重载（比如搜索、条件筛选等），支持复选框，支持分页，支持单元格编辑等等一些列功能。尽管如此，我们仍将对其进行完善，在控制代码量和性能的前提下，不定期增加更多人性化功能。table 模块也将是 layui 重点维护的项目之一。

> 模块加载名称：table

## 快速使用

创建一个 table 实例最简单的方式是，在页面放置一个元素 `<table id="demo"></table>`，然后通过 `table.render()` 方法指定该容器，如下所示：

[](./example/table.html ':include :type=iframe height=350px')

一切都并不会陌生：绑定容器、设置数据接口、在表头设定对应的字段，剩下的…就交给 layui 吧。你的牛刀是否早已饥渴难耐？那么不妨现在就去小试一波吧。数据接口可参考：<a href="/version/v2.6.8/modules/mock/table.json" target="_blank">返回的数据</a>，规则在下文也有讲解。

## 三种初始化渲染方式

在上述“快速使用”的介绍中，你已经初步见证了 table 模块的信手拈来，但其使用方式并不止那一种。我们为了满足各种情况下的需求，对 table 模块做了三种初始化的支持，你可按照个人喜好和实际情况灵活使用。

| 方式 | 机制                                                                    | 适用场景                 |                                                                          |
| :--- | :---------------------------------------------------------------------- | :----------------------- | ------------------------------------------------------------------------ |
| 01.  | [方法渲染](https://www.layui.com/doc/modules/table.html#methodRender)   | 用 JS 方法的配置完成渲染 | （推荐）无需写过多的 HTML，在 JS 中指定原始元素，再设定各项参数即可。    |
| 02.  | [自动渲染](https://www.layui.com/doc/modules/table.html#autoRender)     | HTML 配置，自动渲染      | 无需写过多 JS，可专注于 HTML 表头部分                                    |
| 03.  | [转换静态表格](https://www.layui.com/doc/modules/table.html#parseTable) | 转化一段已有的表格元素   | 无需配置数据接口，在 JS 中指定表格元素，并简单地给表头加上自定义属性即可 |

## 方法渲染

其实这是“自动化渲染”的手动模式，本质类似，只是“方法级渲染”将基础参数的设定放在了 JS 代码中，且原始的 table 标签只需要一个 选择器：

```html
<table id="demo" lay-filter="test"></table>
```

```javascript
var table = layui.table;

//执行渲染
table.render({
    elem: '#demo', //指定原始表格元素选择器（推荐id选择器）
    height: 315, //容器高度
    cols: [{}], //设置表头
    //,…… //更多参数参考右侧目录：基本参数选项
});
```

事实上我们更推荐采用“方法级渲染”的做法，其最大的优势在于你可以脱离 HTML 文件，而专注于 JS 本身。尤其对于项目的频繁改动及发布，其便捷性会体现得更为明显。而究竟它与“自动化渲染”的方式谁更简单，也只能由各位猿猿们自行体味了。

备注：`table.render()` 方法返回一个对象：`var tableIns = table.render(options)`，可用于对当前表格进行“重载”等操作，详见目录：表格重载

## 自动渲染

所谓的自动渲染，即：在一段 table 容器中配置好相应的参数，由 table 模块内部自动对其完成渲染，而无需你写初始的渲染方法。其特点在上文也有阐述。你需要关注的是以下三点：

1. 带有 `class="layui-table"` 的 `<table>` 标签。
2. 对标签设置属性 `lay-data=""` 用于配置一些基础参数
3. 在 `<th>` 标签中设置属性 `lay-data=""` 用于配置表头信息

按照上述的规范写好 table 原始容器后，只要你加载了 layui 的 table 模块，就会自动对其建立动态的数据表格。下面是一个示例：

```html
<table class="layui-table" lay-data="{height:315, url:'/demo/table/user/', page:true, id:'test'}" lay-filter="test">
    <thead>
        <tr>
            <th lay-data="{field:'id', width:80, sort: true}">ID</th>
            <th lay-data="{field:'username', width:80}">用户名</th>
            <th lay-data="{field:'sex', width:80, sort: true}">性别</th>
            <th lay-data="{field:'city'}">城市</th>
            <th lay-data="{field:'sign'}">签名</th>
            <th lay-data="{field:'experience', sort: true}">积分</th>
            <th lay-data="{field:'score', sort: true}">评分</th>
            <th lay-data="{field:'classify'}">职业</th>
            <th lay-data="{field:'wealth', sort: true}">财富</th>
        </tr>
    </thead>
</table>
```

## 转换静态表格

假设你的页面已经存在了一段有内容的表格，它由原始的 table 标签组成，这时你需要赋予它一些动态元素，比如拖拽调整列宽？比如排序等等？那么你同样可以很轻松地去实现。如下所示：

[](./example/table-format.html ':include :type=iframe height=350px')

在前面的“方法渲染”和“自动渲染”两种方式中，你的数据都来源于异步的接口，这可能并不利于所谓的 seo（当然是针对于前台页面）。而在这里，你的数据已和页面同步输出，却仍然可以转换成动态表格，是否感受到一丝惬意呢？

## 基础参数一览表

基础参数并非所有都要出现，有必选也有可选，结合你的实际需求自由设定。基础参数一般出现在以下几种场景中：

场景一：下述方法中的键值即为基础参数项

```javascript
table.render({
    height: 300,
    url: '/api/data',
});
```

场景二：下述 lay-data 里面的内容即为基础参数项，切记：值要用单引号

```html
<table lay-data="{height:300, url:'/api/data'}" lay-filter="demo">
    ……
</table>
```

更多场景：下述 options 即为含有基础参数项的对象

```javascript
table.init('filter', options); //转化静态表格
var tableObj = table.render({});
tableObj.reload(options); //重载表格
```

下面是目前 table 模块所支持的全部参数一览表，我们对重点参数进行了的详细说明，你可以点击下述表格最右侧的“示例”去查看

| 参数           | 类型               | 说明                                                                                                                                                                                                                                                                                                                                                               | 示例值                                                                          |
| :------------- | :----------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------ |
| elem           | String/DOM         | 指定原始 table 容器的选择器或 DOM，方法渲染方式必填                                                                                                                                                                                                                                                                                                                | "#demo"                                                                         |
| cols           | Array              | 设置表头。值是一个二维数组。方法渲染方式必填                                                                                                                                                                                                                                                                                                                       | [详见表头参数](https://www.layui.com/doc/modules/table.html#cols)               |
| url（等）      | -                  | 异步数据接口相关参数。其中 url 参数为必填项                                                                                                                                                                                                                                                                                                                        | [详见异步参数](https://www.layui.com/doc/modules/table.html#async)              |
| toolbar        | String/DOM/Boolean | 开启表格头部工具栏区域，该参数支持四种类型值：toolbar: '#toolbarDemo' *//指向自定义工具栏模板选择器*toolbar: '<div>xxx</div>' *//直接传入工具栏模板字符*toolbar: true *//仅开启工具栏，不显示左侧模板*toolbar: 'default' *//让工具栏左侧显示默认的内置模板*注意： 1. 该参数为 layui 2.4.0 开始新增。 2. 若需要“列显示隐藏”、“导出”、“打印”等功能，则必须开启该参数 | false                                                                           |
| defaultToolbar | Array              | 该参数可自由配置头部工具栏右侧的图标按钮                                                                                                                                                                                                                                                                                                                           | [详见头工具栏图标](https://www.layui.com/doc/modules/table.html#defaultToolbar) |
| width          | Number             | 设定容器宽度。table 容器的默认宽度是跟随它的父元素铺满，你也可以设定一个固定值，当容器中的内容超出了该宽度时，会自动出现横向滚动条。                                                                                                                                                                                                                               | 1000                                                                            |
| height         | Number/String      | 设定容器高度                                                                                                                                                                                                                                                                                                                                                       | [详见 height](https://www.layui.com/doc/modules/table.html#height)              |
| cellMinWidth   | Number             | （layui 2.2.1 新增）全局定义所有常规单元格的最小宽度（默认：60），一般用于列宽自动分配的情况。其优先级低于表头参数中的 minWidth                                                                                                                                                                                                                                    | 100                                                                             |
| done           | Function           | 数据渲染完的回调。你可以借此做一些其它的操作                                                                                                                                                                                                                                                                                                                       | [详见 done 回调](https://www.layui.com/doc/modules/table.html#done)             |
| error          | Function           | 数据请求失败的回调，返回两个参数：错误对象、内容 layui 2.6.0 新增                                                                                                                                                                                                                                                                                                  | -                                                                               |
| data           | Array              | 直接赋值数据。既适用于只展示一页数据，也非常适用于对一段已知数据进行多页展示。                                                                                                                                                                                                                                                                                     | [{}, {}, {}, {}, …]                                                             |
| escape         | Boolean            | 是否开启 xss 字符过滤（默认 false）layui 2.6.8 新增                                                                                                                                                                                                                                                                                                                | true                                                                            |
| totalRow       | Boolean/String     | 是否开启合计行区域                                                                                                                                                                                                                                                                                                                                                 | false                                                                           |
| page           | Boolean/Object     | 开启分页（默认：false）。支持传入一个对象，里面可包含 [laypage](https://www.layui.com/doc/modules/laypage.html#options) 组件所有支持的参数（jump、elem 除外）                                                                                                                                                                                                      | {theme: '#c00'}                                                                 |
| limit          | Number             | 每页显示的条数（默认 10）。值需对应 limits 参数的选项。 注意：_优先级低于 page 参数中的 limit 参数_                                                                                                                                                                                                                                                                | 30                                                                              |
| limits         | Array              | 每页条数的选择项，默认：[10,20,30,40,50,60,70,80,90]。 注意：_优先级低于 page 参数中的 limits 参数_                                                                                                                                                                                                                                                                | [30,60,90]                                                                      |
| loading        | Boolean            | 是否显示加载条（默认 true）。若为 false，则在切换分页时，不会出现加载条。该参数只适用于 url 参数开启的方式                                                                                                                                                                                                                                                         | false                                                                           |
| title          | String             | 定义 table 的大标题（在文件导出等地方会用到）                                                                                                                                                                                                                                                                                                                      | "用户表"                                                                        |
| text           | Object             | 自定义文本，如空数据时的异常提示等。                                                                                                                                                                                                                                                                                                                               | [详见自定义文本](https://www.layui.com/doc/modules/table.html#text)             |
| autoSort       | Boolean            | 默认 true，即直接由 table 组件在前端自动处理排序。 若为 false，则需自主排序，即由服务端返回排序好的数据                                                                                                                                                                                                                                                            | [详见事件排序](https://www.layui.com/doc/modules/table.html#onsort)             |
| initSort       | Object             | 初始排序状态。 用于在数据表格渲染完毕时，默认按某个字段排序。                                                                                                                                                                                                                                                                                                      | [详见初始排序](https://www.layui.com/doc/modules/table.html#initSort)           |
| id             | String             | 设定容器唯一 id。id 是对表格的数据操作方法上是必要的传递条件，它是表格容器的索引，你在下文诸多地方都将会见识它的存在。 另外，若该参数未设置，则默认从 _<table id="test"></table>_ 中的 id 属性值中获取。                                                                                                                                                           | test                                                                            |
| skin（等）     | -                  | 设定表格各种外观、尺寸等                                                                                                                                                                                                                                                                                                                                           | [详见表格风格](https://www.layui.com/doc/modules/table.html#skin)               |

## cols - 表头参数一览表

相信我，在你还尚无法驾驭 layui table 的时候，你的所有焦点都应放在这里，它带引领你完成许多可见和不可见甚至你无法想象的工作。如果你采用的是方法渲染，cols 是一个二维数组，表头参数设定在数组内；如果你采用的自动渲染，表头参数的设定应放在 `<th>` 标签上

| 参数         | 类型           | 说明                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 示例值                                                                   |
| :----------- | :------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------------------------------------------------------- |
| field        | String         | 设定字段名。非常重要，且是表格数据列的唯一标识                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | username                                                                 |
| title        | String         | 设定标题名称                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | 用户名                                                                   |
| width        | Number/String  | 设定列宽，若不填写，则自动分配；若填写，则支持值为：数字、百分比。 请结合实际情况，对不同列做不同设定。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | 200 30%                                                                  |
| minWidth     | Number         | 局部定义当前常规单元格的最小宽度（默认：60），一般用于列宽自动分配的情况。其优先级高于基础参数中的 cellMinWidth                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | 100                                                                      |
| type         | String         | 设定列类型。可选值有：normal（常规列，无需设定）checkbox（复选框列）radio（单选框列，layui 2.4.0 新增）numbers（序号列）space（空列）                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | 任意一个可选值                                                           |
| LAY_CHECKED  | Boolean        | 是否全选状态（默认：false）。必须复选框列开启后才有效，如果设置 true，则表示复选框默认全部选中。                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | true                                                                     |
| fixed        | String         | 固定列。可选值有：_left_（固定在左）、_right_（固定在右）。一旦设定，对应的列将会被固定在左或右，不随滚动条而滚动。 注意：_如果是固定在左，该列必须放在表头最前面；如果是固定在右，该列必须放在表头最后面。_                                                                                                                                                                                                                                                                                                                                                                                                   | left（同 true） right                                                    |
| hide         | Boolean        | 是否初始隐藏列，默认：false。layui 2.4.0 新增                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | true                                                                     |
|              |                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |                                                                          |
| totalRow     | Boolean/String | 是否开启该列的自动合计功能，默认：false。当开启时，则默认由前端自动合计当前行数据。从 layui 2.5.6 开始： 若接口直接返回了合计行数据，则优先读取接口合计行数据，格式如下：`{ "code": 0, "totalRow": { "score": "666" ,"experience": "999" }, "data": [{}, {}], "msg": "", "count": 1000} `如上，在 totalRow 中返回所需统计的列字段名和值即可。 另外，totalRow 字段同样可以通过 parseData 回调来解析成为 table 组件所规定的数据格式。从 layui 2.6.3 开始，如果 totalRow 为一个 string 类型，则可解析为合计行的动态模板，如：`totalRow: '{{ d.TOTAL_NUMS }} 单位'//还比如只取整：'{{ parseInt(d.TOTAL_NUMS) }}' ` | true                                                                     |
| totalRowText | String         | 用于显示自定义的合计文本。layui 2.4.0 新增                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | "合计："                                                                 |
|              |                |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |                                                                          |
| sort         | Boolean        | 是否允许排序（默认：false）。如果设置 true，则在对应的表头显示排序 icon，从而对列开启排序功能。注意：_不推荐对值同时存在“数字和普通字符”的列开启排序，因为会进入字典序比对_。比如：_'贤心' > '2' > '100'_，这可能并不是你想要的结果，但字典序排列算法（ASCII 码比对）就是如此。                                                                                                                                                                                                                                                                                                                                | true                                                                     |
| unresize     | Boolean        | 是否禁用拖拽列宽（默认：false）。默认情况下会根据列类型（type）来决定是否禁用，如复选框列，会自动禁用。而其它普通列，默认允许拖拽列宽，当然你也可以设置 true 来禁用该功能。                                                                                                                                                                                                                                                                                                                                                                                                                                    | false                                                                    |
| edit         | String         | 单元格编辑类型（默认不开启）目前只支持：_text_（输入框）                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | text                                                                     |
| event        | String         | 自定义单元格点击事件名，以便在 [tool](https://www.layui.com/doc/modules/table.html#ontool) 事件中完成对该单元格的业务处理                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | 任意字符                                                                 |
| style        | String         | 自定义单元格样式。即传入 CSS 样式                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | background-color: #5FB878; color: #fff;                                  |
| align        | String         | 单元格排列方式。可选值有：_left_（默认）、_center_（居中）、_right_（居右）                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | center                                                                   |
| colspan      | Number         | 单元格所占列数（默认：1）。一般用于多级表头                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | 3                                                                        |
| rowspan      | Number         | 单元格所占行数（默认：1）。一般用于多级表头                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | 2                                                                        |
| templet      | String         | 自定义列模板，模板遵循 [laytpl](https://www.layui.com/doc/modules/laytpl.html) 语法。这是一个非常实用的功能，你可借助它实现逻辑处理，以及将原始数据转化成其它格式，如时间戳转化为日期字符等                                                                                                                                                                                                                                                                                                                                                                                                                    | [详见自定义模板](https://www.layui.com/doc/modules/table.html#templet)   |
| toolbar      | String         | 绑定工具条模板。可在每行对应的列中出现一些自定义的操作性按钮                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | [详见行工具事件](https://www.layui.com/doc/modules/table.html#onrowtool) |

```javascript
// 方法渲染：
table.render({
    cols: [
        [
            //标题栏
            { checkbox: true },
            { field: 'id', title: 'ID', width: 80 },
            { field: 'username', title: '用户名', width: 120 },
        ],
    ],
});
```

```html
<!-- 它等价于自动渲染： -->
<table class="layui-table" lay-data="{基础参数}" lay-filter="test">
    <thead>
        <tr>
            <th lay-data="{checkbox:true}"></th>
            <th lay-data="{field:'id', width:80}">ID</th>
            <th lay-data="{field:'username', width:180}">用户名</th>
        </tr>
    </thead>
</table>
```

以下是一个二级表头的例子：

```javascript
// JS：
table.render({
    cols: [
        [
            //标题栏
            { field: 'username', title: '联系人', width: 80, rowspan: 2 }, //rowspan即纵向跨越的单元格数
            { field: 'amount', title: '金额', width: 80, rowspan: 2 },
            { align: 'center', title: '地址', colspan: 3 }, //colspan即横跨的单元格数，这种情况下不用设置field和width
        ],
        [
            { field: 'province', title: '省', width: 80 },
            { field: 'city', title: '市', width: 120 },
            { field: 'county', title: '详细', width: 300 },
        ],
    ],
});
```

```html
<!-- 它等价于： -->
<table class="layui-table" lay-data="{基础参数}">
    <thead>
        <tr>
            <th lay-data="{field:'username', width:80}" rowspan="2">联系人</th>
            <th lay-data="{field:'amount', width:120}" rowspan="2">金额</th>
            <th lay-data="{align:'center'}" colspan="3">地址</th>
        </tr>
        <tr>
            <th lay-data="{field:'province', width:80}">省</th>
            <th lay-data="{field:'city', width:120}">市</th>
            <th lay-data="{field:'county', width:300}">详细</th>
        </tr>
    </thead>
</table>
```

需要说明的是，table 组件支持无限极表头，你可按照上述的方式继续扩充。核心点在于 rowspan 和 colspan 两个参数的使用。

## templet - 自定义列模板

类型：String，默认值：无

在默认情况下，单元格的内容是完全按照数据接口返回的 content 原样输出的，如果你想对某列的单元格添加链接等其它元素，你可以借助该参数来轻松实现。这是一个非常实用且强大的功能，你的表格内容会因此而丰富多样。

templet 提供了三种使用方式，请结合实际场景选择最合适的一种：

-   如果自定义模版的字符量太大，我们推荐你采用【方式一】；
-   如果自定义模板的字符量适中，或者想更方便地调用外部方法，我们推荐你采用【方式二】；
-   如果自定义模板的字符量很小，我们推荐你采用【方式三】

**方式一：绑定模版选择器。**

```javascript
table.render({
    cols: [
        [
            { field: 'title', title: '文章标题', width: 200, templet: '#titleTpl' }, //这里的templet值是模板元素的选择器
            { field: 'id', title: 'ID', width: 100 },
        ],
    ],
});
```

```html
<th lay-data="{field:'title', width: 200, templet: '#titleTpl'}">文章标题</th>
<th lay-data="{field:'id', width:100}">ID</th>
```

下述是 templet 对应的模板，它可以存放在页面的任意位置。模板遵循于 laytpl 语法，可读取到返回的所有数据

```html
<script type="text/html" id="titleTpl">
    <a href="/detail/{{d.id}}" class="layui-table-link">{{d.title}}</a>
</script>

<!-- 注意：上述的 {{d.id}}、{{d.title}} 是动态内容，它对应数据接口返回的字段名。除此之外，你还可以读取到以下额外字段： -->
<!-- 序号：{{ d.LAY_INDEX }}  -->
<!-- 当前列的表头信息：{{ d.LAY_COL }} （layui 2.6.8 新增） -->
<!-- 由于模板遵循 laytpl 语法（建议细读 laytpl文档 ），因此在模板中你可以写任意脚本语句（如 if else/for等）： -->
<script type="text/html" id="titleTpl">
    {{# if(d.id < 100){ }}
    <a href="/detail/{{d.id}}" class="layui-table-link">{{d.title}}</a>
    {{# } else { }} {{d.title}}(普通用户) {{# } }}
</script>
```

**方式二：函数转义。templet 若未函数，则返回一个参数 d（包含当前行数据及特定的额外字段）。如下所示：**

```javascript
table.render({
    cols: [
        [
            {
                field: 'title',
                title: '文章标题',
                width: 200,
                templet: function (d) {
                    console.log(d.LAY_INDEX); //得到序号。一般不常用
                    console.log(d.LAY_COL); //得到当前列表头配置信息（layui 2.6.8 新增）。一般不常用

                    //得到当前行数据，并拼接成自定义模板
                    return 'ID：' + d.id + '，标题：<span style="color: #c00;">' + d.title + '</span>';
                },
            },
            { field: 'id', title: 'ID', width: 100 },
        ],
    ],
});
```

**方式三：直接赋值模版字符。事实上，templet 也可以直接是一段 html 内容，如：**

```javascript
var config = {
    templet: '<div><a href="/detail/{{d.id}}" class="layui-table-link">{{d.title}}</a></div>';
}

// 注意：这里一定要被一层 <div></div> 包裹，否则无法读取到模板
```

## toolbar - 绑定工具条模板

类型：String，默认值：无

通常你需要在表格的每一行加上 查看、编辑、删除 这样类似的操作按钮，而 tool 参数就是为此而生，你因此可以非常便捷地实现各种操作功能。tool 参数和 templet 参数的使用方式完全类似，通常接受的是一个选择器，也可以是一段 HTML 字符。

```javascript
table.render({
    cols: [
        [
            { field: 'id', title: 'ID', width: 100 },
            { fixed: 'right', width: 150, align: 'center', toolbar: '#barDemo' }, //这里的toolbar值是模板元素的选择器
        ],
    ],
});
```

```html
<!-- 等价于： -->
<th lay-data="{field:'id', width:100}">ID</th>
<th lay-data="{fixed: 'right', width:150, align:'center', toolbar: '#barDemo'}"></th>
```

下述是 toolbar 对应的模板，它可以存放在页面的任意位置：

```html
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>

    <!-- 这里同样支持 laytpl 语法，如： -->
    {{# if(d.auth > 2){ }}
    <a class="layui-btn layui-btn-xs" lay-event="check">审核</a>
    {{# } }}
</script>

<!-- 注意：属性 lay-event="" 是模板的关键所在，值可随意定义。 -->
```

接下来我们可以借助 table 模块的工具条事件，完成不同的操作功能：

```javascript
//工具条事件
table.on('tool(test)', function (obj) {
    //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
    var data = obj.data; //获得当前行数据
    var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
    var tr = obj.tr; //获得当前行 tr 的 DOM 对象（如果有的话）

    if (layEvent === 'detail') {
        //查看
        //do somehing
    } else if (layEvent === 'del') {
        //删除
        layer.confirm('真的删除行么', function (index) {
            obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
            layer.close(index);
            //向服务端发送删除指令
        });
    } else if (layEvent === 'edit') {
        //编辑
        //do something

        //同步更新缓存对应的值
        obj.update({
            username: '123',
            title: 'xxx',
        });
    } else if (layEvent === 'LAYTABLE_TIPS') {
        layer.alert('Hi，头部工具栏扩展的右侧图标。');
    }
});
```

## 异步数据参数

数据的异步请求由以下几个参数组成：

| 参数名        | 功能                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| :------------ | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| url           | 接口地址。默认会自动传递两个参数：_?page=1&limit=30_（该参数可通过 request 自定义） _page_ 代表当前页码、_limit_ 代表每页数据量                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| method        | 接口 http 请求类型，默认：get                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| where         | 接口的其它参数。如：_where: {token: 'sasasas', id: 123}_                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| contentType   | 发送到服务端的内容编码类型。如果你要发送 json 内容，可以设置：_contentType: 'application/json'_                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| headers       | 接口的请求头。如：_headers: {token: 'sasasas'}_                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|               |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| **parseData** | 数据格式解析的回调函数，用于将返回的任意数据格式解析成 table 组件规定的数据格式。table 组件默认规定的数据格式为（参考：[/demo/table/user/](https://www.layui.com/demo/table/user/?page=1&limit=30)）：`默认规定的数据格式code{ "code": 0, "msg": "", "count": 1000, "data": [{}, {}]} `很多时候，您接口返回的数据格式并不一定都符合 table 默认规定的格式，比如：`假设您返回的数据格式code{ "status": 0, "message": "", "total": 180, "data": { "item": [{}, {}] }} `那么你需要借助 parseData 回调函数将其解析成 table 组件所规定的数据格式`table.render({ elem: '#demp' ,url: '' ,parseData: function(res){ //res 即为原始返回的数据 return { "code": res.status, //解析接口状态 "msg": res.message, //解析提示文本 "count": res.total, //解析数据长度 "data": res.data.item //解析数据列表 }; } //,…… //其他参数}); `该参数非常实用，系 layui 2.4.0 开始新增 |
| request       | 用于对分页请求的参数：page、limit 重新设定名称，如：`table.render({ elem: '#demp' ,url: '' ,request: { pageName: 'curr' //页码的参数名称，默认：page ,limitName: 'nums' //每页数据量的参数名，默认：limit } //,…… //其他参数}); `那么请求数据时的参数将会变为：_?curr=1&nums=30_                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| response      | 您还可以借助 response 参数来重新设定返回的数据格式，如：`table.render({ elem: '#demp' ,url: '' ,response: { statusName: 'status' //规定数据状态的字段名称，默认：code ,statusCode: 200 //规定成功的状态码，默认：0 ,msgName: 'hint' //规定状态信息的字段名称，默认：msg ,countName: 'total' //规定数据总数的字段名称，默认：count ,dataName: 'rows' //规定数据列表的字段名称，默认：data } //,…… //其他参数}); `那么上面所规定的格式为：`重新规定的数据格式code{ "status": 200, "hint": "", "total": 1000, "rows": []} `                                                                                                                                                                                                                                                                                                                                      |

注意：`request` 和 `response` 参数均为 layui 2.1.0 版本新增

调用示例：

```javascript
//“方法级渲染”配置方式
table.render({
    //其它参数在此省略
    url: '/api/data/',
    //where: {token: 'sasasas', id: 123} //如果无需传递额外参数，可不加该参数
    //method: 'post' //如果无需自定义HTTP类型，可不加该参数
    //request: {} //如果无需自定义请求参数，可不加该参数
    //response: {} //如果无需自定义数据响应名称，可不加该参数
});
```

```html
<!-- 等价于（“自动化渲染”配置方式） -->
<table class="layui-table" lay-data="{url:'/api/data/'}" lay-filter="test">
    ……
</table>
```

## done - 数据渲染完的回调

类型：Function，默认值：无

无论是异步请求数据，还是直接赋值数据，都会触发该回调。你可以利用该回调做一些表格以外元素的渲染。

```javascript
table.render({
    //其它参数在此省略
    done: function (res, curr, count) {
        //如果是异步请求数据方式，res即为你接口返回的信息。
        //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
        console.log(res);

        //得到当前页码
        console.log(curr);

        //得到数据总量
        console.log(count);
    },
});
```

## defaultToolbar - 头部工具栏右侧图标

类型：Array，默认值：["filter","exports","print"]

该参数可自由配置头部工具栏右侧的图标按钮，值为一个数组，支持以下可选值：

-   filter: _显示筛选图标_
-   exports: _显示导出图标_
-   print: _显示打印图标_

另外你还可以无限扩展图标按钮`（layui 2.5.5 新增）`：

```javascript
table.render({
    //其它参数在此省略
    defaultToolbar: [
        'filter',
        'print',
        'exports',
        {
            title: '提示', //标题
            layEvent: 'LAYTABLE_TIPS', //事件名，用于 toolbar 事件中使用
            icon: 'layui-icon-tips', //图标类名
        },
    ],
});
```

扩展的图标可通过 toolbar 事件回调（详见行工具事件），其中 layEvent 的值会在事件的回调参数中返回，以便区分不同的触发动作。

## text - 自定义文本

类型：Object

table 模块会内置一些默认的文本信息，如果想修改，你可以设置以下参数达到目的。

```javascript
table.render({
    //其它参数在此省略
    text: {
        none: '暂无相关数据', //默认：无数据。
    },
});
```

## initSort - 初始排序

类型：Object，默认值：无

用于在数据表格渲染完毕时，默认按某个字段排序。注：该参数为 layui 2.1.1 新增

```javascript
//“方法级渲染”配置方式
table.render({
    //其它参数在此省略
    initSort: {
        field: 'id', //排序字段，对应 cols 设定的各字段名
        type: 'desc', //排序方式  asc: 升序、desc: 降序、null: 默认排序
    },
});
```

```html
<!-- 等价于（“自动化渲染”配置方式） -->
<table class="layui-table" lay-data="{initSort:{field:'id', type:'desc'}}" lay-filter="test">
    ……
</table>
```

## height - 设定容器高度

类型：Number/String，可选值如下：

| 可选值    | 说明                                                                                                                                                                                                         | 示例              |
| :-------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :---------------- |
| 不填写    | 默认情况。高度随数据列表而适应，表格容器不会出现纵向滚动条                                                                                                                                                   | -                 |
| 固定值    | 设定一个数字，用于定义容器高度，当容器中的内容超出了该高度时，会自动出现纵向滚动条                                                                                                                           | height: 315       |
| full-差值 | 高度将始终铺满，无论浏览器尺寸如何。这是一个特定的语法格式，其中 _full_ 是固定的，而 _差值_ 则是一个数值，这需要你来预估，比如：表格容器距离浏览器顶部和底部的距离“和” PS：_该功能为 layui 2.1.0 版本中新增_ | height: 'full-20' |

示例：

```javascript
//“方法级渲染”配置方式
table.render({
    //其它参数在此省略
    height: 315, //固定值
});
table.render({
    //其它参数在此省略
    height: 'full-20', //高度最大化减去差值
});
```

```html
<!-- 等价于（“自动化渲染”配置方式） -->
<table class="layui-table" lay-data="{height:315}" lay-filter="test">
    ……
</table>
<table class="layui-table" lay-data="{height:'full-20'}" lay-filter="test">
    ……
</table>
```

## 设定表格外观

控制表格外观的主要由以下参数组成：

| 参数名 | 可选值                                                    | 备注                                             |
| :----- | :-------------------------------------------------------- | :----------------------------------------------- |
| skin   | line （行边框风格） row （列边框风格） nob （无边框风格） | 用于设定表格风格，若使用默认风格不设置该属性即可 |
| even   | true/false                                                | 若不开启隔行背景，不设置该参数即可               |
| size   | sm （小尺寸） lg （大尺寸）                               | 用于设定表格尺寸，若使用默认尺寸不设置该属性即可 |

```javascript
//“方法级渲染”配置方式
table.render({
    //其它参数在此省略
    skin: 'line', //行边框风格
    even: true, //开启隔行背景
    size: 'sm', //小尺寸的表格
});
```

```html
<!-- 等价于（“自动化渲染”配置方式） -->
<table class="layui-table" lay-data="{skin:'line', even:true, size:'sm'}" lay-filter="test">
    ……
</table>
```

## 基础方法

基础用法是 table 模块的关键组成部分，目前所开放的所有方法如下：

```
> table.set(options); //设定全局默认参数。options即各项基础参数
> table.on('event(filter)', callback); //事件。event为内置事件名（详见下文），filter为容器lay-filter设定的值
> table.init(filter, options); //filter为容器lay-filter设定的值，options即各项基础参数。例子见：转换静态表格
> table.checkStatus(id); //获取表格选中行（下文会有详细介绍）。id 即为 id 参数对应的值
> table.render(options); //用于表格方法级渲染，核心方法。应该不用再过多解释了，详见：方法级渲染
> table.reload(id, options, deep); //表格重载
> table.resize(id); //重置表格尺寸
> table.exportFile(id, data, type); //导出数据
> table.getData(id); //用于获取表格当前页的所有行数据（layui 2.6.0 开始新增）
```

## 获取选中行

该方法可获取到表格所有的选中行相关数据

语法：`table.checkStatus('ID')`，其中 ID 为基础参数 id 对应的值（见：设定容器唯一 ID），如：

```html
<!-- 【自动化渲染】 -->
<table class="layui-table" lay-data="{id: 'idTest'}">
    ……
</table>
```

```javascript
// 【方法渲染】
table.render({
    //其它参数省略
    id: 'idTest',
});
```

```javascript
var checkStatus = table.checkStatus('idTest'); //idTest 即为基础参数 id 对应的值

console.log(checkStatus.data); //获取选中行的数据
console.log(checkStatus.data.length); //获取选中行数量，可作为是否有选中行的条件
console.log(checkStatus.isAll); //表格是否全选
```

## 重置表格尺寸

该方法可重置表格尺寸和结构，其内部完成了：固定列高度平铺、动态分配列宽、容器滚动条宽高补丁 等操作。它一般用于特殊情况下（如“非窗口 resize”导致的表格父容器宽度变化而引发的列宽适配异常），以保证表格在此类特殊情况下依旧能友好展示。

语法：`table.resize('ID')`，其中 ID 为基础参数 id 对应的值（见：设定容器唯一 ID），如：

```javascript
table.render({ //其它参数省略
  ,elem: '#demo'
  //,…… //其它参数
  ,id: 'idTest'
});

//执行表格“尺寸结构”的重置，一般写在对应的事件中
table.resize('idTest');
```

## 表格重载

很多时候，你需要对表格进行重载。比如数据全局搜索。以下方法可以帮你轻松实现这类需求（可任选一种）。

| 语法                            | 说明                                                                                                                                                                                                                                                                                            | 适用场景       |
| ------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------- |
| table.reload(ID, options, deep) | 参数 _ID_ 即为基础参数 id 对应的值，见：[设定容器唯一 ID](https://www.layui.com/doc/modules/table.html#id) 参数 _options_ 即为各项基础参数 参数 _deep_：是否采用深度重载（即参数深度克隆，也就是重载时始终携带初始时及上一次重载时的参数），默认 false 注意：deep 参数为 layui 2.6.0 开始新增。 | 所有渲染方式   |
| tableIns.reload(options, deep)  | 参数同上 tableIns 可通过 var tableIns = table.render() 得到                                                                                                                                                                                                                                     | 仅限方法级渲染 |

请注意：如果你在 2.6.0 之前版本已经习惯深度重载模式的，现在请在第三个参数中，加上 true，如：

`table.reload(ID, options, true);` 这样就跟 v2.5.7 及之前版本处理机制完全一样。或者您也可以像下面这样做：

```javascript
//由于 2.6.0 之前的版本是采用深重载，
//所以如果您之前真实采用了深重载机制，那么建议将以下代码放入您的全局位置，从而可对老项目起到兼容作用
var tableReload = table.reload;
table.reload = function () {
    var args = [];
    layui.each(arguments, function (index, item) {
        args.push(item);
    });
    args[2] === undefined && (args[2] = true);
    return tableReload.apply(null, args);
};
//但如果你之前没有采用深重载机制，也可以不放。不放甚至可以解决你之前因为多次 reload 而带来的各种参数重叠问题
```

重载示例：

```html
<!-- 【HTML】 -->
<table class="layui-table" lay-data="{id: 'idTest'}">
    ……
</table>
```

```javascript
// 【JS】
table.reload('idTest', {
    url: '/api/table/search',
    where: {}, //设定异步数据接口的额外参数
    //,height: 300
});
```

```javascript
//所获得的 tableIns 即为当前容器的实例
var tableIns = table.render({
    elem: '#id',
    cols: [], //设置表头
    url: '/api/data', //设置异步接口
    id: 'idTest',
});

//这里以搜索为例
tableIns.reload({
    where: {
        //设定异步数据接口的额外参数，任意设
        aaaaaa: 'xxx',
        bbb: 'yyy',
        //…
    },
    page: {
        curr: 1, //重新从第 1 页开始
    },
});
//上述方法等价于
table.reload('idTest', {
    where: {
        //设定异步数据接口的额外参数，任意设
        aaaaaa: 'xxx',
        bbb: 'yyy',
        //…
    },
    page: {
        curr: 1, //重新从第 1 页开始
    },
}); //只重载数据
```

注意：这里的表格重载是指对表格重新进行渲染，包括数据请求和基础参数的读取

## 导出任意数据

尽管 table 的工具栏内置了数据导出按钮，但有时你可能需要通过方法去导出任意数据，那么可以借助以下方法：

语法：`table.exportFile(id, data, type)`

```javascript
var ins1 = table.render({
    elem: '#demo',
    id: 'test',
    //,…… //其它参数
});

//将上述表格示例导出为 csv 文件
table.exportFile(ins1.config.id, data); //data 为该实例中的任意数量的数据
```

事实上，该方法也可以不用依赖 table 的实例，可直接导出任意数据：

```javascript
table.exportFile(
    ['名字', '性别', '年龄'],
    [
        ['张三', '男', '20'],
        ['李四', '女', '18'],
        ['王五', '女', '19'],
    ],
    'csv'
); //默认导出 csv，也可以为：xls
```

## 事件

语法：`table.on('event(filter)', callback);` 注：event 为内置事件名，filter 为容器 lay-filter 设定的值目前所支持的所有事件见下文

默认情况下，事件所触发的是全部的 table 模块容器，但如果你只想触发某一个容器，使用事件过滤器即可。假设原始容器为：`<table class="layui-table" lay-filter="test"></table>` 那么你的事件写法如下：

```javascript
//以复选框事件为例
table.on('checkbox(test)', function (obj) {
    console.log(obj);
});
```

## 触发头部工具栏事件

点击头部工具栏区域设定了属性为 lay-event="" 的元素时触发（该事件为 layui 2.4.0 开始新增）。如：

```html
原始容器
<table id="demo" lay-filter="test"></table>

工具栏模板：
<script type="text/html" id="toolbarDemo">
  <div class="layui-btn-container">
    <button class="layui-btn layui-btn-sm" lay-event="add">添加</button>
    <button class="layui-btn layui-btn-sm" lay-event="delete">删除</button>
    <button class="layui-btn layui-btn-sm" lay-event="update">编辑</button>
  </div>
</script>

<script;>
//JS 调用：
table.render({
  elem: '#demo'
  ,toolbar: '#toolbarDemo'
  //,…… //其他参数
});

//触发事件
table.on('toolbar(test)', function(obj){
  var checkStatus = table.checkStatus(obj.config.id);
  switch(obj.event){
    case 'add':
      layer.msg('添加');
    break;
    case 'delete':
      layer.msg('删除');
    break;
    case 'update':
      layer.msg('编辑');
    break;
  };
});
</script>
```

## 触发复选框选择

点击复选框时触发，回调函数返回一个 object 参数：

```javascript
table.on('checkbox(test)', function (obj) {
    console.log(obj); //当前行的一些常用操作集合
    console.log(obj.checked); //当前是否选中状态
    console.log(obj.data); //选中行的相关数据
    console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one
});
```

## 触发单选框选择

点击表格单选框时触发，回调函数返回一个 object 参数，携带的成员如下：

```javascript
table.on('radio(test)', function (obj) {
    //test 是 table 标签对应的 lay-filter 属性
    console.log(obj); //当前行的一些常用操作集合
    console.log(obj.checked); //当前是否选中状态
    console.log(obj.data); //选中行的相关数据
});
```

## 触发单元格编辑

单元格被编辑，且值发生改变时触发，回调函数返回一个 object 参数，携带的成员如下：

```javascript
table.on('edit(test)', function (obj) {
    //注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
    console.log(obj.value); //得到修改后的值
    console.log(obj.field); //当前编辑的字段名
    console.log(obj.data); //所在行的所有相关数据
});
```

## 触发行单双击事件

点击或双击行时触发。该事件为 layui 2.4.0 开始新增

```javascript
//触发行单击事件
table.on('row(test)', function (obj) {
    console.log(obj.tr); //得到当前行元素对象
    console.log(obj.data); //得到当前行数据
    //obj.del(); //删除当前行
    //obj.update(fields) //修改当前行数据
});

//触发行双击事件
table.on('rowDouble(test)', function (obj) {
    //obj 同上
});
```

## 触发行中工具条点击事件

具体用法见：绑定工具条

## 触发排序切换

点击表头排序时触发，它通用在基础参数中设置 autoSort: false 时使用，以完成服务端的排序，而不是默认的前端排序。该事件的回调函数返回一个 object 参数，携带的成员如下：

```javascript
//禁用前端自动排序，以便由服务端直接返回排序好的数据
table.render({
    elem: '#id',
    autoSort: false, //禁用前端自动排序。注意：该参数为 layui 2.4.4 新增
    //,… //其它参数省略
});

//触发排序事件
table.on('sort(test)', function (obj) {
    //注：sort 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
    console.log(obj.field); //当前排序的字段名
    console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
    console.log(this); //当前排序的 th 对象

    //尽管我们的 table 自带排序功能，但并没有请求服务端。
    //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
    table.reload('idTest', {
        initSort: obj, //记录初始排序，如果不设的话，将无法标记表头的排序状态。
        where: {
            //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
            field: obj.field, //排序字段
            order: obj.type, //排序方式
        },
    });

    layer.msg('服务端排序。order by ' + obj.field + ' ' + obj.type);
});
```

## 结语

table 模块自推出以来，因某些功能的缺失，一度饱受非议，也背负了各种莫须有的锅，然而我始终未曾放弃对它的优化。为了迎合 layui 开箱即用的理念，我的工作并不是那么轻松。无论是从代码，还是文档和示例的撰写上，我都进行了多次调整，为的只是它能被更多人认可。——贤心

> layui - 在每一个细节中，用心与你沟通
