# 模板引擎文档 - layui.laytpl

> laytpl 是 JavScript 模板引擎，在字符解析上有着比较出色的表现，欠缺之处在于异常调试上。由于传统意义的前端模板引擎已经变得不再流行，所以 laytpl 后续可能会进行重写，目前方向暂时还没有想好，预计会在 layui 比较稳定后开始实施。

> 模块加载名称：laytpl，在线调试：http://www.layui.com/demo/laytpl.html

## 快速使用

laytpl 模板可与数据共存，这意味着可直接在模板中处理逻辑。

```javascript
layui.use('laytpl', function () {
    var laytpl = layui.laytpl;

    //直接解析字符
    laytpl('{{ d.name }}是一位公猿').render(
        {
            name: '贤心',
        },
        function (string) {
            console.log(string); //贤心是一位公猿
        }
    );

    //你也可以采用下述同步写法，将 render 方法的回调函数剔除，可直接返回渲染好的字符
    var string = laytpl('{{ d.name }}是一位公猿').render({
        name: '贤心',
    });
    console.log(string); //贤心是一位公猿

    //如果模板较大，你也可以采用数据的写法，这样会比较直观一些
    laytpl(['{{ d.name }}是一位公猿', '其它字符 {{ d.content }}  其它字符'].join(''));
});
```

你也可以将模板存储在页面或其它任意位置：

```html
//第一步：编写模版。你可以使用一个script标签存放模板，如：
<script id="demo" type="text/html">
    <h3>{{ d.title }}</h3>
    <ul>
        {{# layui.each(d.list, function(index, item){ }}
        <li>
            <span>{{ item.modname }}</span>
            <span>{{ item.alias }}：</span>
            <span>{{ item.site || '' }}</span>
        </li>
        {{# }); }} {{# if(d.list.length === 0){ }} 无数据 {{# } }}
    </ul>
</script>

//第二步：建立视图。用于呈现渲染结果。
<div id="view"></div>

//第三步：渲染模版
<script type="text/javascript">
    var data = {
        //数据
        title: 'Layui常用模块',
        list: [
            { modname: '弹层', alias: 'layer', site: 'layer.layui.com' },
            { modname: '表单', alias: 'form' },
        ],
    };
    var getTpl = demo.innerHTML,
        view = document.getElementById('view');
    laytpl(getTpl).render(data, function (html) {
        view.innerHTML = html;
    });
</script>
```

| 语法                    | 说明                                                                                                                         | 示例                                                                                                                       |
| :---------------------- | :--------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------- |
| {{ d.field }}           | 输出一个普通字段，不转义 html                                                                                                | `<div>{{ d.content }}</div> `                                                                                              |
| {{= d.field }}          | 输出一个普通字段，并转义 html                                                                                                | `<h2>{{= d.title }}</h2>`                                                                                                  |
| {{# JavaScript表达式 }} | JS 语句。一般用于逻辑处理。用分隔符加 # 号开头。 注意：如果你是想输出一个函数，正确的写法是：{{ fn() }}，而不是：{{# fn() }} | `{{# var fn = function(){ return '2017-08-18'; }; }} {{# if(true){ }} 开始日期：{{ fn() }}{{# } else { }} 已截止{{# } }} ` |
| {{! template !}}        | 对一段指定的模板区域进行过滤，即不解析该区域的模板。注：layui 2.1.6 新增                                                     | `<div> {{! 这里面的模板不会被解析 !}}</div>`                                                                               |

## 分隔符

如果模版默认的 `{{ }}` 分隔符与你的其它模板（一般是服务端模板）存在冲突，你也可以重新定义分隔符：

```javascript
laytpl.config({
    open: '<%',
    close: '%>',
});

//分割符将必须采用上述定义的
laytpl(
    [
        '<%# var type = "公"; %>', //JS 表达式
        '<% d.name %>是一位<% type %>猿。',
    ].join('')
).render(
    {
        name: '贤心',
    },
    function (string) {
        console.log(string); //贤心是一位公猿
    }
);
```

## 结语

laytpl 应用在 layui 的很多模块中，如：layim、table 等。尽管传统意义的前端模板引擎已经变得不再流行，但 laytpl 仍然可以发挥一定作用，不妨尝试一下吧。

> layui - 在每一个细节中，用心与你沟通
