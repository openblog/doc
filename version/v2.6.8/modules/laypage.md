# 分页模块文档 - layui.laypage

> layPage 致力于提供极致的分页逻辑，既可轻松胜任异步分页，也可作为页面刷新式分页。自 layui 2.0 开始，无论是从核心代码还是 API 设计，layPage 都完成了一次蜕变。清爽的 UI、灵活的排版，极简的调用方式，这一切的优质元素，都将毫无违和感地镶嵌在你的页面之中。

> 模块加载名称：laypage

## 快速使用

laypage 的使用非常简单，指向一个用于存放分页的容器，通过服务端得到一些初始值，即可完成分页渲染：

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>layPage快速使用</title>
        <link rel="stylesheet" href="/static/build/layui.css" media="all" />
    </head>
    <body>
        <div id="test1"></div>

        <script src="/static/build/layui.js"></script>
        <script>
            layui.use('laypage', function () {
                var laypage = layui.laypage;

                //执行一个laypage实例
                laypage.render({
                    elem: 'test1', //注意，这里的 test1 是 ID，不用加 # 号
                    count: 50, //数据总数，从服务端得到
                });
            });
        </script>
    </body>
</html>
```

## 基础参数选项

通过核心方法：`laypage.render(options)` 来设置基础参数。由于使用非常简单，本篇直接罗列核心接口的参数选项

| 参数选项 | 说明                                                                                                                                                                                                                       | 类型           | 默认值                   |
| :------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------- | :----------------------- |
| elem     | 指向存放分页的容器，值可以是容器 ID、DOM 对象。如： 1. elem: 'id' _注意：这里不能加 # 号_ 2. elem: document.getElementById('id')                                                                                           | String/Object  | -                        |
| count    | 数据总数。一般通过服务端得到                                                                                                                                                                                               | Number         | -                        |
| limit    | 每页显示的条数。laypage 将会借助 count 和 limit 计算出分页数。                                                                                                                                                             | Number         | 10                       |
| limits   | 每页条数的选择项。如果 layout 参数开启了 limit，则会出现每页条数的 select 选择框                                                                                                                                           | Array          | [10, 20, 30, 40, 50]     |
| curr     | 起始页。一般用于刷新类型的跳页以及 HASH 跳页。如：`</>code//开启location.hash的记录laypage.render({ elem: 'test1' ,count: 500 ,curr: location.hash.replace('#!fenye=', '') //获取起始页 ,hash: 'fenye' //自定义hash值}); ` | Number         | 1                        |
| groups   | 连续出现的页码个数                                                                                                                                                                                                         | Number         | 5                        |
| prev     | 自定义“上一页”的内容，支持传入普通文本和 HTML                                                                                                                                                                              | String         | 上一页                   |
| next     | 自定义“下一页”的内容，同上                                                                                                                                                                                                 | String         | 下一页                   |
| first    | 自定义“首页”的内容，同上                                                                                                                                                                                                   | String         | 1                        |
| last     | 自定义“尾页”的内容，同上                                                                                                                                                                                                   | String         | 总页数值                 |
| layout   | 自定义排版。可选值有：_count_（总条目输区域）、_prev_（上一页区域）、_page_（分页区域）、_next_（下一页区域）、_limit_（条目选项区域）、_refresh_（页面刷新区域。注意：layui 2.3.0 新增） 、_skip_（快捷跳页区域）         | Array          | ['prev', 'page', 'next'] |
| theme    | 自定义主题。支持传入：_颜色值_，或*任意普通字符*。如： 1. theme: '#c00' 2. theme: 'xxx' //将会生成 class="layui-laypage-xxx" 的 CSS 类，以便自定义主题                                                                     | String         | -                        |
| hash     | 开启 location.hash，并自定义 hash 值。如果开启，在触发分页时，会自动对 url 追加：_#!hash 值={curr}_ 利用这个，可以在页面载入时就定位到指定页                                                                               | String/Boolean | false                    |

## jump - 切换分页的回调

当分页被切换时触发，函数返回两个参数：`obj（当前分页的所有选项值）、first（是否首次，一般用于初始加载的判断）`

```javascript
laypage.render({
    elem: 'test1',
    count: 70, //数据总数，从服务端得到
    jump: function (obj, first) {
        //obj包含了当前分页的所有参数，比如：
        console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
        console.log(obj.limit); //得到每页显示的条数

        //首次不执行
        if (!first) {
            //do something
        }
    },
});
```

## 结束

laypage 只负责分页本身的逻辑，具体的数据请求与渲染需要另外去完成。laypage 不仅能应用在一般的异步分页上，还可直接对一段已知数据进行分页展现。其中大家使用最多的 table 组件就是采用的 laypage 作为分页依赖。

> layui - 在每一个细节中，用心与你沟通
