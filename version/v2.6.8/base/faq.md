# 常见问题

> 本篇将主要讲解使用过程中普遍遇到的“问题”，并非是 BUG，通常是需要我们自己去注意的一些点。（持续补充）

## 哪里有 layui 未压缩源代码？

之所以在下载包里没有提供未压缩的源代码，是为了方便直接用于生产环境。layui 源代码可通过以下平台获取：

[Github](https://github.com/sentsin/layui/) [Gitee](https://gitee.com/sentsin/layui/)

## 应该如何加载模块？

```javascript
layui.use(["layer", "form", "element"], function () {
    var layer = layui.layer,
        form = layui.form,
        element = layui.element;

    //……
    //你的代码都应该写在这里面
});
```

## 为什么表单不显示？

当你使用表单时，layui 会对 select、checkbox、radio 等原始元素隐藏，从而进行美化修饰处理。但如果您的表单元素是动态添加的，那么在组件初始加载的时候是无法读取到的，这是你只需执行一个视图渲染的实例即可。[#详见说明](./module/form)

同理的还有 [element 模块](./module/element)

## 遇到各种问题怎么办？

求助 Gitee 开发者社区，抱团取暖

[issues](https://gitee.com/sentsin/layui/issues)

layui - 在每一个细节中，用心与你沟通
