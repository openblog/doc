# 底层方法

> 本篇主要介绍基础库所发挥的作用，其中过滤了大部分在外部不常用的方法，侧重罗列了基础框架支撑。

## 全局配置

方法：`layui.config(options)`

你可以在使用模块之前，全局化配置一些参数，尽管大部分时候它不是必须的。所以我们目前提供的全局配置项非常少，这也是为了减少一些不必要的工作，尽可能让使用变得更简单。目前支持的全局配置项如下：

```javascript
layui.config({
    dir: "/res/layui/", //layui.js 所在目录（如果是 script 单独引入 layui.js，无需设定该参数）一般可无视
    version: false, //一般用于更新模块缓存，默认不开启。设为 true 即让浏览器不缓存。也可以设为一个固定的值，如：201610
    debug: false, //用于开启调试模式，默认 false，如果设为 true，则JS模块的节点会保留在页面
    base: "", //设定扩展的 layui 模块的所在目录，一般用于外部模块扩展
});
```

如果你对 layui.js 本身进行了动态加载等其他特殊场景，那么上述 layui.config 所设定的 dir 参数会因此失效，它会在部分组件的依赖文件（css）加载后才执行，此时你可以在动态加载 layui.js 之前预先定义一个我们约定好的全局对象：

```html
<script>
    var LAYUI_GLOBAL = {
        dir: "/res/layui/", //layui.js 所在目录（layui 2.6.6 开始新增）
    };
</script>
```

> 提示：以上 dir 参数的目录设定仅针对特殊场景，如是采用 script 标签正常引入 layui 的，可以无视该 dir 参数。

## 定义模块

方法：`layui.define([mods], callback)`

通过该方法可在新的 JS 文件中定义一个 layui 模块。参数 mods 是可选的，用于声明该模块所依赖的模块。callback 即为模块加载完毕的回调函数，它返回一个 exports 参数，用于输出该模块的接口。

```javascript
/** demo.js **/
layui.define(function (exports) {
    //do something

    exports("demo", {
        msg: "Hello Demo",
    });
});
```

跟 RequireJS 最大不同的地方在于接口输出，exports 是一个函数，它接受两个参数，第 1 个参数为模块名，第 2 个参数为模块接口。当你声明了上述的一个模块后，你就可以在外部使用了，demo 就会注册到 layui 对象下，即可通过 `var demo = layui.demo` 去得到该模块接口。你也可以在定义一个模块的时候，声明该模块所需的依赖，如：

```javascript
/** demo.js **/
layui.define(["layer", "laypage", "mod1"], function (exports) {
    //此处 mod1 为你的任意扩展模块
    //do something

    exports("demo", {
        msg: "Hello Demo",
    });
});
```

上述的 `layer、laypage` 都是 layui 的内置模块。

## 加载模块

方法：`layui.use([mods], callback)`

参数 mods：如果填写，必须是一个 layui 合法的模块名（不能包含目录）。

-   从 layui 2.6 开始，若 mods 不填，只传一个 callback 参数，则表示引用所有内置模块。

参数 callback：即为模块加载完毕的回调函数。

-   从 layui 2.6 开始，该回调会在 html 文档加载完毕后再执行，确保你的代码在任何地方都能对元素进行操作。

```javascript
//引用指定模块
layui.use(["layer", "laydate"], function () {
    var layer = layui.layer,
        laydate = layui.laydate;

    //do something
});

//引用所有模块（layui 2.6 开始支持）
layui.use(function () {
    var layer = layui.layer,
        laydate = layui.laydate,
        table = layui.table;
    //…

    //do something
});
```

你还可以通过回调函数得到模块对象，如

```javascript
//通过回调的参数得到模块对象
layui.use(["layer", "laydate", "table"], function (layer, laydate, table) {
    //使用 layer
    layer.msg("test");

    //使用 laydate
    laydate.render({});

    //使用 table
    table.render({});
});
```

## 动态加载 CSS

方法：`layui.link(href)`

href 即为 css 路径。注意：该方法并非是你使用 layui 所必须的，它一般只是用于动态加载你的外部 CSS 文件。

## 本地存储

本地存储是对 localStorage 和 sessionStorage 的友好封装，可更方便地管理本地数据。

-   localStorage 持久化存储：`layui.data(table, settings)`，数据会永久存在，除非物理删除。

-   sessionStorage 会话性存储：`layui.sessionData(table, settings)`，页面关闭后即失效。注：layui 2.2.5 新增

上述两个方法的使用方式是完全一样的。其中参数 table 为表名，settings 是一个对象，用于设置 key、value。下面以 layui.data 方法为例：

```javascript
//【增】：向 test 表插入一个 nickname 字段，如果该表不存在，则自动建立。
layui.data("test", {
    key: "nickname",
    value: "贤心",
});

//【删】：删除 test 表的 nickname 字段
layui.data("test", {
    key: "nickname",
    remove: true,
});
layui.data("test", null); //删除test表

//【改】：同【增】，会覆盖已经存储的数据

//【查】：向 test 表读取全部的数据
var localTest = layui.data("test");
console.log(localTest.nickname); //获得“贤心”
```

## 获取浏览器信息

方法：`layui.device(key)`，参数 key 是可选的

由于 layui 的一些功能进行了兼容性处理和响应式支持，因此该方法同样发挥了至关重要的作用。其返回的信息如下：

```javascript
var device = layui.device();
```

device 即可根据不同的设备返回下述不同的信息

```json
{
    "os": "windows", //当前浏览器所在的底层操作系统，如：Windows、Linux、Mac 等
    "ie": false, //当前浏览器是否为 ie6-11 的版本，如果不是 ie 浏览器，则为 false
    "weixin": false, //当前浏览器是否为微信 App 环境
    "android": false, //当前浏览器是否为安卓系统环境
    "ios": false, //当前浏览器是否为 IOS 系统环境
    "mobile": false //当前浏览器是否为移动设备环境（v2.5.7 新增）
}
```

有时你的 App 可能会对 userAgent 插入一段特定的标识，譬如：

```
Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 myapp/1.8.6 Safari/537.36
```

你要验证当前的 WebView 是否在你的 App 环境，即可通过上述的 myapp（即为 Native 给 Webview 插入的标识，可以随意定义）来判断。

```javascript
var device = layui.device("myapp");
if (device.myapp) {
    alert("在我的App环境");
}
```

## 其它底层方法

除上述介绍的方法外，layui 内部还提供了许多底层引擎，他们同样是整个 layui 框架的有力支撑，在日常应用中通常也会用到：

| 方法/属性                                | 描述                                                                                                                                                                                                              |
| :--------------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| layui.cache                              | 静态属性。获得一些配置及临时的缓存信息                                                                                                                                                                            |
| layui.extend(options)                    | 拓展一个模块别名，如：layui.extend({test: '/res/js/test'})                                                                                                                                                        |
| layui.each(obj, fn)                      | 对象（Array、Object、DOM 对象等）遍历，可用于取代 for 语句                                                                                                                                                        |
| layui.\_typeof(operand)                  | 获取详细数据类型（基本数据类型和各类常见引用类型）如：`layui._typeof([]); //array` `layui._typeof({}); //object` `layui._typeof(new Date()); //date`等等。该方法为 layui 2.6.7 新增                               |
| layui.\_isArray(obj)                     | 对象是否为泛数组结构。如 Array、NodeList、jQuery 对象等等。`layui._isArray([1,6]); //true` `layui._isArray($('div')); //true` `layui._isArray(document.querySelectorAll('div')); //true`该方法为 layui 2.6.7 新增 |
| layui.getStyle(node, name)               | 获得一个原始 DOM 节点的 style 属性值，如： `layui.getStyle(document.body, 'font-size')`                                                                                                                           |
| layui.img(url, callback, error)          | 图片预加载                                                                                                                                                                                                        |
| layui.sort(obj, key, desc)               | 将数组中的对象按某个成员重新对该数组排序，如： `layui.sort([{a: 3},{a: 1},{a: 5}], 'a')`                                                                                                                          |
| layui.router()                           | 获得 location.hash 路由结构，一般在单页面应用中发挥作用。                                                                                                                                                         |
| layui.url(href)                          | 用于将一段 URL 链接中的 pathname、search、hash 属性值进行对象化处理参数： _href_ 可选。若不传，则自动读取当前页面的 url（即：location.href） 示例：`var url = layui.url();`                                       |
| layui.hint()                             | 向控制台打印一些异常信息，目前只返回了 error 方法： `layui.hint().error('出错啦');`                                                                                                                               |
| layui.stope(e)                           | 阻止事件冒泡                                                                                                                                                                                                      |
| layui.onevent(modName, events, callback) | 增加自定义模块事件。有兴趣的同学可以阅读 layui.js 源码以及 form 模块                                                                                                                                              |
| layui.event(modName, events, params)     | 执行自定义模块事件，搭配 onevent 使用                                                                                                                                                                             |
| layui.off(events, modName)               | 用于移除模块相关事件的监听（v2.5.7 新增）如：`layui.off('select(filter)', 'form')`， 那么 `form.on('select(filter)', callback)`的事件将会被移除。                                                                 |
| layui.factory(modName)                   | 用于获取模块对应的 define 回调函数                                                                                                                                                                                |

## 第三方工具

layui **部分模块**依赖 _jQuery_（比如 layer），但是你并不用去额外加载 jQuery。layui 已经将 jQuery 最稳定的一个版本改为 layui 的内部模块，当你去使用 layer 之类的模块时，它会首先判断你的页面是否已经引入了 jQuery，如果没有，则加载内部的 jQuery 模块，如果有，则不会加载。

另外，我们的图标取材于阿里巴巴矢量图标库（_iconfont_），构建工具采用 _Gulp_ 。除此之外，不依赖于任何第三方工具。

> layui - 在每一个细节中，用心与你沟通
