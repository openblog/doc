# 进度条 - 页面元素

> 进度条可应用于许多业务场景，如任务完成进度、loading 等等，是一种较为直观的表达元素。

> 依赖加载组件：[element](.../modules/element)

## 常规用法

<div class="layui-progress">
    <div class="layui-progress-bar" lay-percent="20%" style="width: 20%;"></div>
</div>
<br/>
<div class="layui-progress">
    <div class="layui-progress-bar layui-bg-orange" lay-percent="50%" style="width: 50%;"></div>
</div>
<br/>
<div class="layui-progress" lay-showpercent="true">
    <div class="layui-progress-bar layui-bg-blue" lay-percent="80%" style="width: 80%;"><span class="layui-progress-text">80%</span></div>
</div>
<br/>
<div class="layui-progress layui-progress-big" lay-showpercent="true">
    <div class="layui-progress-bar layui-bg-cyan" lay-percent="30%" style="width: 30%;"><span class="layui-progress-text">30%</span></div>
</div>

我们进度条提供了两种尺寸及多种颜色的显示风格，其中颜色的选值可参考：[背景色公共类](element)。基本元素结构如下

```html
<div class="layui-progress">
    <div class="layui-progress-bar" lay-percent="10%"></div>
</div>

<script>
    //注意进度条依赖 element 模块，否则无法进行正常渲染和功能性操作
    layui.use('element', function () {
        var element = layui.element;
    });
</script>
```

属性 lay-percent ：代表进度条的初始百分比，你也可以动态改变进度，详见：进度条的动态操作

正如上述你见到的，当对元素设置了 class 为 `layui-progress-big` 时，即为大尺寸的进度条风格。默认风格的进度条的百分比如果开启，会在右上角显示，而大号进度条则会在内部显示。

## 显示进度比文本

<div class="layui-progress" lay-showpercent="true">
    <div class="layui-progress-bar layui-bg-red" lay-percent="1/3" style="width: 33.3333%;"><span class="layui-progress-text">1/3</span></div>
</div>
<br/>
<div class="layui-progress" lay-showpercent="true">
    <div class="layui-progress-bar layui-bg-red" lay-percent="80%" style="width: 80%;"><span class="layui-progress-text">80%</span></div>
</div>
<br/>
<div class="layui-progress layui-progress-big" lay-showpercent="true">
    <div class="layui-progress-bar layui-bg-green" lay-percent="50%" style="width: 50%;"><span class="layui-progress-text">50%</span></div>
</div>

通过对父级元素设置属性 `lay-showPercent="yes"` 来开启进度比的文本显示，支持：普通数字、百分数、分数（layui 2.1.7 新增）

```html
<div class="layui-progress" lay-showPercent="true">
    <div class="layui-progress-bar layui-bg-red" lay-percent="1/3"></div>
</div>

<div class="layui-progress" lay-showPercent="yes">
    <div class="layui-progress-bar layui-bg-red" lay-percent="30%"></div>
</div>

<div class="layui-progress layui-progress-big" lay-showPercent="yes">
    <div class="layui-progress-bar layui-bg-green" lay-percent="50%"></div>
</div>
```

注意：默认情况下不会显示百分比文本，如果你想开启，对属性 lay-showPercent 设置任意值即可，如：yes。但如果不想显示，千万别设置 no 或者 false，直接剔除该属性即可。

## 大号进度条

<div class="layui-progress layui-progress-big">
  <div class="layui-progress-bar" lay-percent="20%" style="width: 20%;"></div>
</div>
 <br/>
<div class="layui-progress layui-progress-big">
  <div class="layui-progress-bar layui-bg-orange" lay-percent="50%" style="width: 50%;"></div>
</div>
 <br/>
<div class="layui-progress layui-progress-big" lay-showPercent="true">
  <div class="layui-progress-bar layui-bg-blue" lay-percent="80%" style="width: 80%;"></div>
</div>

和世界上许多客观存在的事物一样，进度条也不仅是只有短小细长的尺寸，当然也有大而粗，这是我们认为相对合适的大尺寸

```html
<div class="layui-progress layui-progress-big">
    <div class="layui-progress-bar ayui-bg-green" lay-percent="20%"></div>
</div>

<div class="layui-progress layui-progress-big">
    <div class="layui-progress-bar layui-bg-orange" lay-percent="50%"></div>
</div>

<div class="layui-progress layui-progress-big" lay-showPercent="true">
    <div class="layui-progress-bar layui-bg-blue" lay-percent="80%"></div>
</div>
```

正如上述你见到的，当对元素设置了 class 为 `layui-progress-big` 时，即为大尺寸的进度条风格。默认风格的进度条的百分比如果开启，会在右上角显示，而大号进度条则会在内部显示。

如果你需要对进度条进行动态操作，如动态改变进度，那么你需要阅读：[element](.../modules/element)模块

> layui - 在每一个细节中，用心与你沟通
