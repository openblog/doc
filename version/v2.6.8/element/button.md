# 按钮 - 页面元素

> 向任意 HTML 元素设定 class="layui-btn"，建立一个基础按钮。通过追加格式为 layui-btn-{type}的 class 来定义其它按钮风格。内置的按钮 class 可以进行任意组合，从而形成更多种按钮风格。

## 用法

```html
<button type="button" class="layui-btn">一个标准的按钮</button>
```

```html
<a href="http://www.layui.com" class="layui-btn">一个可跳转的按钮</a>
```

## 主题

<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-primary">原始按钮</button>
    <button type="button" class="layui-btn">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-normal">百搭按钮</button>
    <button type="button" class="layui-btn layui-btn-warm">暖色按钮</button>
    <button type="button" class="layui-btn layui-btn-danger">警告按钮</button>
    <button type="button" class="layui-btn layui-btn-disabled">禁用按钮</button>
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-primary">原始按钮</button>
    <button type="button" class="layui-btn">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-normal">百搭按钮</button>
    <button type="button" class="layui-btn layui-btn-warm">暖色按钮</button>
    <button type="button" class="layui-btn layui-btn-danger">警告按钮</button>
    <button type="button" class="layui-btn layui-btn-disabled">禁用按钮</button>
</div>
```

| 名称 | 组合                                 |
| :--- | :----------------------------------- |
| 原始 | class="layui-btn layui-btn-primary"  |
| 默认 | class="layui-btn"                    |
| 百搭 | class="layui-btn layui-btn-normal"   |
| 暖色 | class="layui-btn layui-btn-warm"     |
| 警告 | class="layui-btn layui-btn-danger"   |
| 禁用 | class="layui-btn layui-btn-disabled" |

---

<div class="layui-btn-container">
    <a href="" class="layui-btn layui-btn-primary layui-border-green">主色按钮</a>
    <button class="layui-btn layui-btn-primary layui-border-blue">百搭按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-orange">暖色按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-red">警告按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-black">深色按钮</button>
</div>

```html
<div class="layui-btn-container">
    <a href="" class="layui-btn layui-btn-primary layui-border-green">主色按钮</a>
    <button class="layui-btn layui-btn-primary layui-border-blue">百搭按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-orange">暖色按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-red">警告按钮</button>
    <button class="layui-btn layui-btn-primary layui-border-black">深色按钮</button>
</div>
```

| 名称 | 组合                                                    |
| :--- | :------------------------------------------------------ |
| 主色 | class="layui-btn layui-btn-primary layui-border-green"  |
| 百搭 | class="layui-btn layui-btn-primary layui-border-blue"   |
| 暖色 | class="layui-btn layui-btn-primary layui-border-orange" |
| 警告 | class="layui-btn layui-btn-primary layui-border-red"    |
| 深色 | class="layui-btn layui-btn-primary layui-border-black"  |

## 尺寸

<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg">大型按钮</button>
    <button type="button" class="layui-btn">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-sm">小型按钮</button>
    <button type="button" class="layui-btn layui-btn-xs">迷你按钮</button>
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg">大型按钮</button>
    <button type="button" class="layui-btn">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-sm">小型按钮</button>
    <button type="button" class="layui-btn layui-btn-xs">迷你按钮</button>
</div>
```

| 尺寸 | 组合                           |
| :--- | :----------------------------- |
| 大型 | class="layui-btn layui-btn-lg" |
| 默认 | class="layui-btn"              |
| 小型 | class="layui-btn layui-btn-sm" |
| 迷你 | class="layui-btn layui-btn-xs" |

<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg layui-btn-normal">大型百搭</button>
    <button type="button" class="layui-btn layui-btn-warm">正常暖色</button>
    <button type="button" class="layui-btn layui-btn-sm layui-btn-danger">小型警告</button>
    <button type="button" class="layui-btn layui-btn-xs layui-btn-disabled">禁用</button>
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg layui-btn-normal">大型百搭</button>
    <button type="button" class="layui-btn layui-btn-warm">正常暖色</button>
    <button type="button" class="layui-btn layui-btn-sm layui-btn-danger">小型警告</button>
    <button type="button" class="layui-btn layui-btn-xs layui-btn-disabled">禁用</button>
</div>
```

| 尺寸     | 组合                                              |
| :------- | :------------------------------------------------ |
| 大型百搭 | class="layui-btn layui-btn-lg layui-btn-normal"   |
| 正常暖色 | class="layui-btn layui-btn-warm"                  |
| 小型警告 | class="layui-btn layui-btn-sm layui-btn-danger"   |
| 迷你禁用 | class="layui-btn layui-btn-xs layui-btn-disabled" |

<button type="button" class="layui-btn layui-btn-fluid">流体按钮（最大化适应）</button>

<button type="button" class="layui-btn layui-btn-primary layui-btn-fluid">流体按钮（最大化适应）</button>

```html
<button type="button" class="layui-btn layui-btn-fluid">流体按钮（最大化适应）</button>
```

## 圆角

<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-radius layui-btn-primary">原始按钮</button>
    <button type="button" class="layui-btn layui-btn-radius">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-normal">百搭按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-warm">暖色按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-danger">警告按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-disabled">禁用按钮</button>
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-radius layui-btn-primary">原始按钮</button>
    <button type="button" class="layui-btn layui-btn-radius">默认按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-normal">百搭按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-warm">暖色按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-danger">警告按钮</button>
    <button type="button" class="layui-btn layui-btn-radius layui-btn-disabled">禁用按钮</button>
</div>
```

| 主题 | 组合                                                  |
| :--- | :---------------------------------------------------- |
| 原始 | class="layui-btn layui-btn-radius layui-btn-primary"  |
| 默认 | class="layui-btn layui-btn-radius"                    |
| 百搭 | class="layui-btn layui-btn-radius layui-btn-normal"   |
| 暖色 | class="layui-btn layui-btn-radius layui-btn-warm"     |
| 警告 | class="layui-btn layui-btn-radius layui-btn-danger"   |
| 禁用 | class="layui-btn layui-btn-radius layui-btn-disabled" |

---

<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">大型百搭</button>
    <button type="button" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger">小型警告</button>
    <button type="button" class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled">迷你禁用</button>
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal">大型百搭</button>
    <button type="button" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger">小型警告</button>
    <button type="button" class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled">迷你禁用</button>
</div>
```

| 尺寸     | 组合                                                               |
| :------- | :----------------------------------------------------------------- |
| 大型百搭 | class="layui-btn layui-btn-lg layui-btn-radius layui-btn-normal"   |
| 小型警告 | class="layui-btn layui-btn-sm layui-btn-radius layui-btn-danger"   |
| 迷你禁用 | class="layui-btn layui-btn-xs layui-btn-radius layui-btn-disabled" |

哈哈哈哈哈，这组合名长得简直没朋友~

## 按钮组

<div class="layui-btn-group">
    <button type="button" class="layui-btn">增加</button>
    <button type="button" class="layui-btn">编辑</button>
    <button type="button" class="layui-btn">删除</button>
</div>

<div class="layui-btn-group">
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
</div>

<div class="layui-btn-group">
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon"></i>
    </button>
</div>

将按钮放入一个 `class="layui-btn-group"` 元素中，即可形成按钮组，按钮本身仍然可以随意搭配

```html
<div class="layui-btn-group">
    <button type="button" class="layui-btn">增加</button>
    <button type="button" class="layui-btn">编辑</button>
    <button type="button" class="layui-btn">删除</button>
</div>

<div class="layui-btn-group">
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon">&#xe654;</i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon">&#xe642;</i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon">&#xe640;</i>
    </button>
    <button type="button" class="layui-btn layui-btn-sm">
        <i class="layui-icon">&#xe602;</i>
    </button>
</div>

<div class="layui-btn-group">
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon">&#xe654;</i>
    </button>
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon">&#xe642;</i>
    </button>
    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm">
        <i class="layui-icon">&#xe640;</i>
    </button>
</div>
```

## 按钮容器

尽管按钮在同节点并排时会自动拉开间距，但在按钮太多的情况，效果并不是很美好。因为你需要用到按钮容器

<div class="layui-btn-container">
    <button type="button" class="layui-btn">按钮一</button> 
    <button type="button" class="layui-btn">按钮二</button> 
    <button type="button" class="layui-btn">按钮三</button> 
    <button type="button" class="layui-btn">按钮四</button> 
    <button type="button" class="layui-btn">按钮五</button> 
    <button type="button" class="layui-btn">按钮六</button>
    <button type="button" class="layui-btn">按钮一</button> 
    <button type="button" class="layui-btn">按钮二</button> 
    <button type="button" class="layui-btn">按钮三</button> 
    <button type="button" class="layui-btn">按钮四</button> 
    <button type="button" class="layui-btn">按钮五</button> 
    <button type="button" class="layui-btn">按钮六</button>
    <button type="button" class="layui-btn">按钮一</button> 
    <button type="button" class="layui-btn">按钮二</button> 
    <button type="button" class="layui-btn">按钮三</button> 
    <button type="button" class="layui-btn">按钮四</button> 
    <button type="button" class="layui-btn">按钮五</button> 
    <button type="button" class="layui-btn">按钮六</button> 
    <button type="button" class="layui-btn">按钮一</button> 
    <button type="button" class="layui-btn">按钮二</button> 
</div>

```html
<div class="layui-btn-container">
    <button type="button" class="layui-btn">按钮一</button>
    <button type="button" class="layui-btn">按钮二</button>
    <button type="button" class="layui-btn">按钮三</button>
</div>
```

## 结语

你是否发现，主题、尺寸、图标、圆角的交叉组合，可以形成难以计算的按钮种类。另外，你可能最关注的是配色，Layui 内置的六种主题的按钮颜色都是业界常用的标准配色，如果他们仍然无法与你的网站契合，那么请先允许我“噗”一声。。。然后你就大胆地自撸一个颜色吧！比如：<i style="color: #E46AA3;">粉红色</i>或者<i style="color: #A67727;">菊花色</i>（一个有味道的颜色）

> layui - 在每一个细节中，用心与你沟通
