# 字体图标

> layui 的所有图标全部采用字体形式，取材于阿里巴巴矢量图标库（iconfont）。因此你可以把一个 icon 看作是一个普通的文字，这意味着你直接用 css 控制文字属性，如 color、font-size，就可以改变图标的颜色和大小。你可以通过 font-class 或 unicode 来定义不同的图标。

## 使用方式

通过对一个内联元素（一般推荐用 i 标签）设定 `class="layui-icon"`，来定义一个图标，然后对元素加上图标对应的 font-class（注意：layui 2.3.0 之前只支持采用 unicode 字符)，即可显示出你想要的图标，譬如：

从 layui 2.3.0 开始，支持 font-class 的形式定义图标：

```html
<i class="layui-icon layui-icon-face-smile"></i>
```

注意：在 layui 2.3.0 之前的版本，只能设置 unicode 来定义图标

```html
<i class="layui-icon">&#xe60c;</i>
```

其中的 &#xe60c; 即是图标对应的 unicode 字符

你可以去定义它的颜色或者大小，如： <i class="layui-icon layui-icon-face-smile" style="font-size: 30px; color: #1E9FFF;"></i>

```
<i class="layui-icon layui-icon-face-smile" style="font-size: 30px; color: #1E9FFF;"></i>
```

## 内置图标一览表（168 个）

[](./example/iconfont.html ':include :type=iframe height=600px')

## 跨域问题的解决

由于浏览器存在同源策略，所以如果 layui（里面含图标字体文件）所在的地址与你当前的页面地址不在同一个域下，即会出现图标跨域问题。所以要么你就把 layui 与网站放在同一服务器，要么就对 layui 所在的资源服务器的 Response Headers 加上属性：`Access-Control-Allow-Origin: *`

> layui - 在每一个细节中，用心与你沟通
