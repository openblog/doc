# 面板 - 页面元素

> 一般的面板通常是指一个独立的容器，而折叠面板则能有效地节省页面的可视面积，非常适合应用于：QA 说明、帮助文档等

> 依赖加载组件：[element](.../modules/element)

## 常规面板

[](./example/panel.html ':include :type=iframe height=200px')

```html
<div class="layui-row layui-col-space15">
    <div class="layui-col-md6">
        <div class="layui-panel">
            <div style="padding: 30px;">一个面板</div>
        </div>
    </div>
    <div class="layui-col-md6">
        <div class="layui-panel">
            <div style="padding: 30px;">一个面板</div>
        </div>
    </div>
</div>
```

!> 提示：该功能为 layui 2.6.0 新增

## 卡片面板

[](./example/panel-card.html ':include :type=iframe height=200px')

```html
<div class="layui-card">
    <div class="layui-card-header">卡片面板</div>
    <div class="layui-card-body">
        卡片式面板面板通常用于非白色背景色的主体内<br />
        从而映衬出边框投影
    </div>
</div>
```

如果你的网页采用的是默认的白色背景，不建议使用卡片面板。

## 折叠面板

[](./example/panel-collapse.html ':include :type=iframe height=320px')

通过对内容元素设置 class 为 `layui-show` 来选择性初始展开某一个面板，element 模块会自动呈现状态图标。

```html
<div class="layui-collapse">
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">杜甫</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">李清照</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">鲁迅</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
</div>

<script>
    //注意：折叠面板 依赖 element 模块，否则无法进行功能性操作
    layui.use('element', function () {
        var element = layui.element;

        //…
    });
</script>
```

## 开启手风琴

[](./example/panel-collapse-accordion.html ':include :type=iframe height=320px')

在折叠面板的父容器设置属性 `lay-accordion` 来开启手风琴，那么在进行折叠操作时，始终只会展现当前的面板。

```html
<div class="layui-collapse" lay-accordion>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">杜甫</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">李清照</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
    <div class="layui-colla-item">
        <h2 class="layui-colla-title">鲁迅</h2>
        <div class="layui-colla-content layui-show">内容区域</div>
    </div>
</div>
```

> layui - 在每一个细节中，用心与你沟通
