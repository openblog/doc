# CSS3 动画类

> 在实用价值的前提之下，我们并没有内置过多花俏的动画。而他们同样在 layui 的许多交互元素中，发挥着重要的作用。layui 的动画全部采用 CSS3，因此不支持 ie8 和部分不支持 ie9。

## 使用方式

动画的使用非常简单，直接对元素赋值动画特定的 class 类名即可。如：

其中 layui-anim 是必须的，后面跟着的即是不同的动画类

```html
<div class="layui-anim layui-anim-up"></div>
```

循环动画，追加：layui-anim-loop

```html
<div class="layui-anim layui-anim-up layui-anim-loop"></div>
```

内置 CSS3 动画一览表 下面是不同的动画类名，点击下述绿色块，可直接预览动画

[](./example/animate.html ':include :type=iframe height=800px')

## 结语

物不在多，有用则精。

> layui - 在每一个细节中，用心与你沟通
