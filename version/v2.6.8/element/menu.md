# 基础菜单 - 页面元素

> 菜单是页面必不可少的元素，我们希望它是通用的，所以在结构上，它的组成极其灵活。而事实上，在基础菜单还没有正式推出之前，垂直导航（layui-nav-tree）曾顶替了它的角色，尤其是在管理系统中发挥了举足轻重的作用。尽管它们本质上都属于「菜单」的范畴，但我们姑且约定将水平的称之为「导航」，垂直的称之为正统的「基础菜单」。它将十分有用，许多业务场景都能看到它的存在。

> 可依赖的模块：[dropdown](.../modules/dropdown)

## 菜单结构

通常基础菜单是搭配面板（layui-panel）使用的，但这并不是必须的。基础菜单有它独有的样式结构，以下是一个基本的示例：

[](./example/menu.html ':include :type=iframe height=400px')

```html
<div class="layui-panel">
    <ul class="layui-menu" id="docDemoMenu1">
        <li lay-options="{id: 100}">
            <div class="layui-menu-body-title">menu item 1</div>
        </li>
        <li lay-options="{id: 101}">
            <div class="layui-menu-body-title">
                <a href="#">menu item 2 <span class="layui-badge-dot"></span></a>
            </div>
        </li>
        <li class="layui-menu-item-divider"></li>
        <li class="layui-menu-item-group layui-menu-item-down" lay-options="{type: 'group'}">
            <div class="layui-menu-body-title">menu item 3 group <i class="layui-icon layui-icon-up"></i></div>
            <ul>
                <li lay-options="{id: 1031}">menu item 3-1</li>
                <li lay-options="{id: 1032}">
                    <div class="layui-menu-body-title">menu item 3-2</div>
                </li>
            </ul>
        </li>
        <li class="layui-menu-item-divider"></li>
        <li lay-options="{id: 104}">
            <div class="layui-menu-body-title">menu item 4</div>
        </li>
        <li class="layui-menu-item-parent" lay-options="{type: 'parent'}">
            <div class="layui-menu-body-title">
                menu item 5
                <i class="layui-icon layui-icon-right"></i>
            </div>
            <div class="layui-panel layui-menu-body-panel">
                <ul>
                    <li lay-options="{id: 1051}">
                        <div class="layui-menu-body-title">menu item 5-1</div>
                    </li>
                    <li lay-options="{id: 1051}">
                        <div class="layui-menu-body-title">menu item 5-2</div>
                    </li>
                </ul>
            </div>
        </li>
        <li lay-options="{id: 106}">
            <div class="layui-menu-body-title">menu item 6</div>
        </li>
    </ul>
</div>
```

可以看到，它实则是一个 ul li 的层级嵌套，当需要出现「菜单组」和「子级菜单」时，只需要添加相对应的 class 类名即可。

## 菜单项类型

菜单项是由 li 元素组成的，以下将列举几种被支持的菜单项类型

| 类型         | html                                                                                         | 说明                                                                   |
| :----------- | :------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------- |
| 常规菜单项   | `<li></li> `                                                                                 | 无需添加特定 class                                                     |
| 可收缩菜单组 | `<li class="layui-menu-item-group layui-menu-item-down" lay-options="{type: 'group'}"></li>` | 其中*layui-menu-item-down*为初始展开；*layui-menu-item-up*为初始收缩。 |
| 横向父子菜单 | `<li class="layui-menu-item-parent" lay-options="{type: 'parent'}"></li>`                    | 其子级菜单的结构参照上文的「菜单结构」，主要是需包含一层面板元素。     |
| 分割线       | `<li class="layui-menu-item-divider"></li>`                                                  | 一条横线，用于更好地划分菜单区域                                       |

其子级菜单遵循的列表类型也是一样的。需要注意的是，「可收缩菜单组」的子菜单仅需要再嵌套一层 ul 即可；而「横向父子菜单」还需要套一层 `div class="layui-panel layui-menu-body-panel"`，以便让子菜单层次更加分明。

## 菜单项参数

在上文中，您可能已经多次注意到 `lay-options="{}"` 这个属性了，它正是我们所说的菜单项参数。当点击菜单列表时，回调中将会返回该属性所配置的全部参数，其中 type 参数是组件内部约定的，其它参数可以随意命名。如

```html
<li
    lay-options="{
  id: 100
  ,title: 'menu item 1'
  ,type: '' //支持的类型有：group、parent，具体用法参见上文
  ,aaa: '任意参数'
}"
>
    内容
</li>
```

## 事件触发

我们的 `dropdown` 组件不仅菜单的基本交互（如点击选中、菜单组展开收缩等）进行了支持，还提供了菜单的事件机制：

```javascript
layui.use('dropdown', function () {
    var dropdown = layui.dropdown;

    //菜单点击事件，其中 docDemoMenu1 对应的是菜单结构上的 id 指
    dropdown.on('click(docDemoMenu1)', function (options) {
        var othis = $(this); //当前菜单列表的 DOM 对象
        console.log(options); //菜单列表的 lay-options 中的参数
    });
});
```

## 结语

本篇所讲解的是基础菜单的静态展示，其提供的是一个基础的结构，事实上它还被动态封装在诸如下拉菜单、右键菜单等场景中。

> layui - 在每一个细节中，用心与你沟通
