# layui 颜色设计感

> 视觉疲劳的形成往往是由于颜色过于丰富或过于单一形成的麻木感，而 layui 提供的颜色，清新而不乏深沉，互相柔和，不过分刺激大脑皮层的神经反应，形成越久越耐看的微妙影像。合理搭配，可与各式各样的网站避免违和，从而使你的 Web 平台看上去更为融洽。

## 主色调

<p style="background-color: #009688; padding: 5px 15px; color: #fff;">主色调之一</p>
<p style="background-color: #5FB878; padding: 5px 15px; color: #fff;">一般用于选中状态</p>
<p style="background-color: #393D49; padding: 5px 15px; color: #fff;">通常用于导航</p>
<p style="background-color: #1E9FFF; padding: 5px 15px; color: #fff;">经典蓝</p>

layui 主要是以象征包容的墨绿作为主色调，由于它给人以深沉感，所以通常会以浅黑色的作为其陪衬，又会以蓝色这种比较鲜艳的色调来弥补它的色觉疲劳，整体让人清新自然，愈发耐看。【取色意义】：我们执着于务实，不盲目攀比，又始终不忘绽放活力。这正是 layui 所追求的价值。

## 次色调

<p style="background-color: #FFB800; padding: 5px 15px; color: #fff;">暖色系</p>

<p style="background-color: #FF5722; padding: 5px 15px; color: #fff;">比较引人注意的颜色</p>

<p style="background-color: #01AAED; padding: 5px 15px; color: #fff;">文本链接着色</p>

<p style="background-color: #2F4056; padding: 5px 15px; color: #fff;">侧边色</p>

事实上，layui 并非不敢去尝试一些亮丽的颜色，但许多情况下一个它可能并不是那么合适，所以我们把这些颜色归为“场景色”，即按照实际场景来呈现对应的颜色，比如你想给人以警觉感，可以尝试用上面的红色。他们一般会出现在 layui 的按钮、提示和修饰性元素，以及一些侧边元素上。

## 中性色

他们一般用于背景、边框等

<p style="background-color: #FAFAFA; padding: 5px 15px;">#FAFAFA</p>
<p style="background-color: #F6F6F6; padding: 5px 15px;">#F6F6F6</p>
<p style="background-color: #eeeeee; padding: 5px 15px;">#eeeeee</p>
<p style="background-color: #e2e2e2; padding: 5px 15px;">#e2e2e2</p>
<p style="background-color: #dddddd; padding: 5px 15px;">#dddddd</p>
<p style="background-color: #d2d2d2; padding: 5px 15px;">#d2d2d2</p>
<p style="background-color: #cccccc; padding: 5px 15px;">#cccccc</p>
<p style="background-color: #c2c2c2; padding: 5px 15px;">#c2c2c2</p>

layui 认为灰色系代表极简，因为这是一种神奇的颜色，几乎可以与任何元素搭配，不易形成视觉疲劳，且永远不会过时。低调而优雅！

## 七种色

layui 内置了七种背景色，以便你用于各种元素中，如：徽章、分割线、导航等等

<p class="layui-bg-red" style="padding: 5px; 15px;">赤色：class="layui-bg-red"</p>
<p class="layui-bg-orange" style="padding: 5px; 15px;">橙色：class="layui-bg-orange"</p>
<p class="layui-bg-green" style="padding: 5px; 15px;">墨绿：class="layui-bg-green"</p>
<p class="layui-bg-cyan" style="padding: 5px; 15px;">藏青：class="layui-bg-cyan"</p>
<p class="layui-bg-blue" style="padding: 5px; 15px;">蓝色：class="layui-bg-blue"</p>
<p class="layui-bg-black" style="padding: 5px; 15px;">雅黑：class="layui-bg-black"</p>
<p class="layui-bg-gray" style="padding: 5px; 15px;">银灰：class="layui-bg-gray"</p>

## 结语

“不热衷于视觉设计的程序猿不是一个好作家！” ——贤心

> layui - 在每一个细节中，用心与你沟通
